package com.meevii.adsdk.applovin_plugin;

import android.content.Context;

import com.applovin.adview.AppLovinInterstitialAd;
import com.applovin.adview.AppLovinInterstitialAdDialog;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdVideoPlaybackListener;
import com.meevii.adsdk.base_plugin.adcommon.IAdListener;
import com.meevii.adsdk.base_plugin.adcommon.ThreadHandler;

public class Interstitial {

    private AppLovinInterstitialAdDialog interstitialAd;
    private IAdListener adListener;
    private boolean isLoaded;
    private String adUnitId;
    private AppLovinAd currentAd;

    public Interstitial(final Context context, final String adUnitId, IAdListener adListener) {

        this.adListener = adListener;
        this.isLoaded = false;
        this.adUnitId = adUnitId;

        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                interstitialAd = AppLovinInterstitialAd.create(AndroidBridge.getAppLovinSdk(), context);
                registerEvent();
            }
        });
    }

    private void registerEvent() {
        interstitialAd.setAdClickListener(new AppLovinAdClickListener() {
            @Override
            public void adClicked(AppLovinAd appLovinAd) {
                if (adListener != null) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            if (adListener != null) {
                                adListener.onAdClick();
                            }
                        }
                    }).start();
                }
            }
        });

        interstitialAd.setAdDisplayListener(new AppLovinAdDisplayListener() {
            @Override
            public void adDisplayed(AppLovinAd appLovinAd) {
                if (adListener != null) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            if (adListener != null) {
                                adListener.onAdOpened();
                            }
                        }
                    }).start();
                }
            }

            @Override
            public void adHidden(AppLovinAd appLovinAd) {
                if (adListener != null) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            if (adListener != null) {
                                adListener.onAdClosed();
                            }
                        }
                    }).start();
                }
            }
        });

        interstitialAd.setAdLoadListener(new AppLovinAdLoadListener() {
            @Override
            public void adReceived(AppLovinAd appLovinAd) {
                if (adListener != null) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            if (adListener != null) {
                                adListener.onAdLoaded();
                            }
                        }
                    }).start();
                }
            }

            @Override
            public void failedToReceiveAd(final int errorCode) {
                if (adListener != null) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            if (adListener != null) {
                                adListener.onAdFailedToLoad(ErrorCode.CoverErrorCode(errorCode), errorCode);
                            }
                        }
                    }).start();
                }
            }
        });

        interstitialAd.setAdVideoPlaybackListener(new AppLovinAdVideoPlaybackListener() {
            @Override
            public void videoPlaybackBegan(AppLovinAd appLovinAd) {
            }

            @Override
            public void videoPlaybackEnded(AppLovinAd appLovinAd, double percentViewed, boolean wasFullyViewed) {
            }
        });
    }

    public void loadAd(Context context) {
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                AndroidBridge.getAppLovinSdk().getAdService().loadNextAdForZoneId(adUnitId, new AppLovinAdLoadListener() {

                    @Override
                    public void adReceived(AppLovinAd appLovinAd) {
                        isLoaded = true;
                        currentAd = appLovinAd;
                        if (adListener != null) {
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    if (adListener != null) {
                                        adListener.onAdLoaded();
                                    }
                                }
                            }).start();
                        }
                    }

                    @Override
                    public void failedToReceiveAd(int errorCode) {
                        if (adListener != null) {
                            final int _errorCode = errorCode;
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    if (adListener != null) {
                                        adListener.onAdFailedToLoad(ErrorCode.CoverErrorCode(_errorCode), _errorCode);
                                    }
                                }
                            }).start();
                        }
                    }
                });
            }
        });
    }


    public boolean isLoaded(Context context) {
        return isLoaded;
    }

    public void show(Context context) {
        if (currentAd == null || !isLoaded(null))
            return;
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                if (isLoaded(null)) {
                    interstitialAd.showAndRender(currentAd);
                }
            }
        });
    }

    public void destroy(Context context) {
        this.adListener = null;
    }
}
