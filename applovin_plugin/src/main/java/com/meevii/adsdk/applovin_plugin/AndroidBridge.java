package com.meevii.adsdk.applovin_plugin;

import android.app.Activity;
import android.content.Context;

import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinSdkSettings;
import com.meevii.adsdk.base_plugin.adcommon.BaseBridge;
import com.meevii.adsdk.base_plugin.adcommon.IAdListener;

public class AndroidBridge extends BaseBridge {

    private static AdObjectFactory objFactorys;
    public static AppLovinSdk sdk;

    private static AppLovinSdkSettings sdkSettings;
    private static String sdkKey;

    public static AppLovinSdk getAppLovinSdk() {
        return sdk;
    }

    public static void SetSdkKey(String key) {
        sdkKey = key;
    }

    public static void InitializeSdk(Activity currentActivity) {
        if (!sdkInitialized(currentActivity)) {
            AppLovinSdk.initializeSdk(currentActivity);
        }
    }

    public static void SetVerboseLoggingOn(String isVerboseLogging) {
        if (sdkSettings == null) {
            sdkSettings = new AppLovinSdkSettings();
        }
        sdkSettings.setVerboseLogging(Boolean.parseBoolean(isVerboseLogging));
    }

    private static boolean sdkInitialized(Activity currentActivity) {
        if (currentActivity == null) {
            return false;
        } else {
            if (sdk == null) {
                sdk = getAppropriateSdkInstance(currentActivity);
            }
            return sdk != null;
        }
    }

    private static AppLovinSdk getAppropriateSdkInstance(Activity currentActivity) {
        if (sdkSettings == null) {
            sdkSettings = new AppLovinSdkSettings();
        }
        return sdkKey != null ? AppLovinSdk.getInstance(sdkKey, sdkSettings, currentActivity) : AppLovinSdk.getInstance(currentActivity);
    }

    public static String CreateInstance(Activity activity, String type, String adUintId, IAdListener listener) {
        if (objFactorys == null) {
            objFactorys = new AdObjectFactory();
        }

        return objFactorys.createInstance(activity, type, adUintId, listener);
    }

    public static Object Invoke(Context context, String className, String objUUId, String methodName) {
        if (objFactorys == null) {
            return null;
        }
        Object obj = objFactorys.getObject(objUUId);
        if (obj == null) {
            return null;
        }
        return BaseBridge.InvokeMethod(obj, context, className, methodName);
    }
}
