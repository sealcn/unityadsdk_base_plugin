package com.meevii.adsdk.applovin_plugin;

import android.content.Context;
import android.view.View;

import com.applovin.adview.AppLovinAdView;
import com.applovin.adview.AppLovinAdViewDisplayErrorCode;
import com.applovin.adview.AppLovinAdViewEventListener;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdSize;
import com.meevii.adsdk.base_plugin.adcommon.AdBanner;
import com.meevii.adsdk.base_plugin.adcommon.AdBannerSize;

public class Banner extends AdBanner {

    private AppLovinAdView mAdView;

    private Banner(Context context, AdBannerSize adSize) {
        super(context, adSize);
    }

    public Banner(Context applicationContext, String adUnitId, AdBannerSize adSize) {
        super(applicationContext, adSize);
        create(adUnitId);
    }


    @Override
    protected View CreateInstance(Context context, String adId, AdBannerSize adSize) {
        mAdView = new AppLovinAdView(AndroidBridge.getAppLovinSdk(), AppLovinAdSize.BANNER, adId, context);
        return mAdView;
    }

    @Override
    protected void setListener(View adView) {

        mAdView.setAdLoadListener(new AppLovinAdLoadListener() {
            @Override
            public void adReceived(final AppLovinAd ad) {
                onAdLoaded();
            }

            @Override
            public void failedToReceiveAd(final int errorCode) {
                onAdFailedToLoad(ErrorCode.CoverErrorCode(errorCode), errorCode);
            }
        });

        mAdView.setAdDisplayListener(new AppLovinAdDisplayListener() {
            @Override
            public void adDisplayed(final AppLovinAd ad) {
                onAdOpened();
            }

            @Override
            public void adHidden(final AppLovinAd ad) {
            }
        });

        mAdView.setAdClickListener(new AppLovinAdClickListener() {
            @Override
            public void adClicked(final AppLovinAd ad) {
            }
        });

        mAdView.setAdViewEventListener(new AppLovinAdViewEventListener() {
            @Override
            public void adOpenedFullscreen(final AppLovinAd ad, final AppLovinAdView adView) {
            }

            @Override
            public void adClosedFullscreen(final AppLovinAd ad, final AppLovinAdView adView) {
            }

            @Override
            public void adLeftApplication(final AppLovinAd ad, final AppLovinAdView adView) {
                onAdLeftApplication();
            }

            @Override
            public void adFailedToDisplay(final AppLovinAd ad, final AppLovinAdView adView, final AppLovinAdViewDisplayErrorCode code) {
            }
        });
    }

    @Override
    protected void doLoadAd(View adView) {
        mAdView.loadNextAd();
    }

    @Override
    protected void resumeAdView(View adView) {
        mAdView.resume();
    }

    @Override
    protected void pauseAdView(View adView) {
        mAdView.pause();
    }

    @Override
    protected void doDestroy(View adView) {
        mAdView.destroy();
    }
}