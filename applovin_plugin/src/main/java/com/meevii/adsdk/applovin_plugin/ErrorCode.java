package com.meevii.adsdk.applovin_plugin;

import com.applovin.sdk.AppLovinErrorCodes;

import static com.applovin.sdk.AppLovinErrorCodes.FETCH_AD_TIMEOUT;
import static com.applovin.sdk.AppLovinErrorCodes.NO_NETWORK;

/**
 * Utilities for the Google Mobile Ads Unity plugin.
 */
public class ErrorCode {
    public static String CoverErrorCode(int errorCode) {
        if (errorCode == AppLovinErrorCodes.NO_FILL)
            return "no fill";
        if (errorCode == AppLovinErrorCodes.INVALID_ZONE || errorCode == -100)
            return "invalid zone";
        if (errorCode == NO_NETWORK)
            return "net work error";
        if (errorCode == FETCH_AD_TIMEOUT)
            return "fetch ad timeout";
        return "" + errorCode;
    }
}
