package com.meevii.adsdk.applovin_plugin;

import android.app.Activity;
import android.content.Context;

import com.applovin.adview.AppLovinIncentivizedInterstitial;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdRewardListener;
import com.applovin.sdk.AppLovinAdVideoPlaybackListener;
import com.applovin.sdk.AppLovinErrorCodes;
import com.meevii.adsdk.base_plugin.adcommon.ADSDKLog;
import com.meevii.adsdk.base_plugin.adcommon.IAdListener;
import com.meevii.adsdk.base_plugin.adcommon.IRewardAdListener;
import com.meevii.adsdk.base_plugin.adcommon.ThreadHandler;

import java.util.Map;

public class RewardVideo {

    private AppLovinIncentivizedInterstitial incentivizedInterstitial;
    private IAdListener adListener;
    private boolean isLoaded;
    AppLovinAd currentAd;
    AppLovinAdRewardListener adRewardListener;
    AppLovinAdVideoPlaybackListener adVideoPlaybackListener;
    AppLovinAdDisplayListener adDisplayListener;
    AppLovinAdClickListener adClickListener;

    private boolean isGotReward = false;

    public RewardVideo(Activity activity, final String adUnitId, IAdListener adListener) {

        this.adListener = adListener;
        this.isLoaded = false;

        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                incentivizedInterstitial = AppLovinIncentivizedInterstitial.create(adUnitId, AndroidBridge.getAppLovinSdk());
                registerEvent();
            }
        });
    }

    private void registerEvent() {

        // Reward Listener
        adRewardListener = new AppLovinAdRewardListener() {

            @Override
            public void userRewardVerified(AppLovinAd appLovinAd, Map map) {
                ADSDKLog.Log("[applovin] userRewardVerified");
                isGotReward = true;
            }

            @Override
            public void userOverQuota(AppLovinAd appLovinAd, Map map) {
            }

            @Override
            public void userRewardRejected(AppLovinAd appLovinAd, Map map) {
            }

            @Override
            public void validationRequestFailed(AppLovinAd appLovinAd, int responseCode) {
                if (responseCode == AppLovinErrorCodes.INCENTIVIZED_USER_CLOSED_VIDEO) {
                    ADSDKLog.Log("[applovin] validationRequestFailed: INCENTIVIZED_USER_CLOSED_VIDEO");
                    isGotReward = false;
                } else if (responseCode == AppLovinErrorCodes.INCENTIVIZED_SERVER_TIMEOUT || responseCode == AppLovinErrorCodes.INCENTIVIZED_UNKNOWN_SERVER_ERROR) {
                    ADSDKLog.Log("[applovin] validationRequestFailed: INCENTIVIZED_SERVER_TIMEOUT / INCENTIVIZED_UNKNOWN_SERVER_ERROR");
                } else if (responseCode == AppLovinErrorCodes.INCENTIVIZED_NO_AD_PRELOADED) {
                    ADSDKLog.Log("[applovin] validationRequestFailed: INCENTIVIZED_NO_AD_PRELOADED");
                }
            }

            @Override
            public void userDeclinedToViewAd(AppLovinAd appLovinAd) {
                ADSDKLog.Log("[applovin] userDeclinedToViewAd");
            }
        };

        adVideoPlaybackListener = new AppLovinAdVideoPlaybackListener() {
            @Override
            public void videoPlaybackBegan(AppLovinAd appLovinAd) {
            }

            @Override
            public void videoPlaybackEnded(AppLovinAd appLovinAd, double percentViewed, boolean fullyWatched) {
            }
        };

        adDisplayListener = new AppLovinAdDisplayListener() {
            @Override
            public void adDisplayed(AppLovinAd appLovinAd) {
                if (adListener != null) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            if (adListener != null) {
                                adListener.onAdOpened();
                            }
                        }
                    }).start();
                }
            }

            @Override
            public void adHidden(AppLovinAd appLovinAd) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        if (adListener != null) {
                            if (adListener instanceof IRewardAdListener) {
                                if (isGotReward) {
                                    ((IRewardAdListener) adListener).onAdReward();
                                } else {
                                    ((IRewardAdListener) adListener).onAdNotReward();
                                }
                            }
                            isGotReward = false;
                            adListener.onAdClosed();
                        }
                    }
                }).start();
            }
        };

        adClickListener = new AppLovinAdClickListener() {
            @Override
            public void adClicked(AppLovinAd appLovinAd) {
                if (adListener != null) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            if (adListener != null) {
                                adListener.onAdClick();
                            }
                        }
                    }).start();
                }
            }
        };
    }

    public void loadAd(Context context) {

        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                incentivizedInterstitial.preload(new AppLovinAdLoadListener() {
                    @Override
                    public void adReceived(AppLovinAd appLovinAd) {
                        ADSDKLog.Log("[applovin] reward onAdLoaded");
                        isLoaded = true;
                        currentAd = appLovinAd;
                        if (adListener != null) {
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    if (adListener != null) {
                                        adListener.onAdLoaded();
                                    }
                                }
                            }).start();
                        }
                    }

                    @Override
                    public void failedToReceiveAd(int errorCode) {
                        ADSDKLog.Log("[applovin] reward failedToReceiveAd" + errorCode);
                        if (adListener != null) {
                            final int _errorCode = errorCode;
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    if (adListener != null) {
                                        adListener.onAdFailedToLoad(ErrorCode.CoverErrorCode(_errorCode), _errorCode);
                                    }
                                }
                            }).start();
                        }
                    }
                });
            }
        });
    }

    public boolean isLoaded(Context context) {
        return isLoaded;
    }

    public void show(final Context context) {
        if (currentAd == null || !isLoaded(null))
            return;
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                if (isLoaded(null)) {
                    incentivizedInterstitial.show(context, adRewardListener, adVideoPlaybackListener, adDisplayListener, adClickListener);
                }
            }
        });
    }

    public void destroy(Context context) {
        this.adListener = null;
        isLoaded = false;
    }
}
