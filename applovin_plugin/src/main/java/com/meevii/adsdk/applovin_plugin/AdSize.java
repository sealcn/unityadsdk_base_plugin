package com.meevii.adsdk.applovin_plugin;

import android.content.Context;
import android.util.DisplayMetrics;
import android.util.TypedValue;

import static android.util.TypedValue.COMPLEX_UNIT_DIP;

public final class AdSize {
    public static final int FULL_WIDTH = -1;
    public static final int AUTO_HEIGHT = -2;
    public static final AdSize BANNER = new AdSize(FULL_WIDTH, 50, "320x50_mb");
    public static final AdSize SMART_BANNER = new AdSize(FULL_WIDTH, AUTO_HEIGHT, "smart_banner");

    private final int width;
    private final int height;
    private final String name;

    public AdSize(int width, int height) {

        this.width = width;
        this.height = height;

        String xDescribe = width == FULL_WIDTH ? "FULL" : String.valueOf(width);
        String yDescribe = height == AUTO_HEIGHT ? "AUTO" : String.valueOf(height);

        this.name = xDescribe + "x" + yDescribe;
        ;
    }

    public AdSize(int width, int height, String name) {

        this.width = width;
        this.height = height;
        this.name = name;
        ;
    }


    public final int getHeight() {
        return this.height;
    }

    public final int getWidth() {
        return this.width;
    }


    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        } else if (!(obj instanceof AdSize)) {
            return false;
        } else {
            AdSize adSize = (AdSize) obj;
            return this.width == adSize.width && this.height == adSize.height && this.name.equals(adSize.name);
        }
    }

    private static int calcuFitHeight(DisplayMetrics dm) {
        int height = (int) ((float) dm.heightPixels / dm.density);
        if (height <= 400) {
            return 32;
        } else {
            return height <= 720 ? 50 : 90;
        }
    }


    public static int getPxFromDP(DisplayMetrics dm, int dp) {
        return (int) TypedValue.applyDimension(COMPLEX_UNIT_DIP, (float) dp, dm);
    }

    public final int getHeightInPixels(Context context) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();

        switch (this.height) {
            case AUTO_HEIGHT:
                return (int) ((float) calcuFitHeight(dm) * dm.density);
            default:
                return getPxFromDP(dm, this.height);
        }
    }

    public final int getWidthInPixels(Context context) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        switch (this.width) {
            case FULL_WIDTH:
                return dm.widthPixels;
            default:
                return getPxFromDP(dm, this.width);
        }
    }

    public final boolean isAutoHeight() {
        return this.height == AUTO_HEIGHT;
    }

    public final boolean isFullWidth() {
        return this.width == FULL_WIDTH;
    }

    public final String toString() {
        return this.name;
    }
}
