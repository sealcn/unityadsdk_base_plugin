package com.meevii.adsdk.chartboost_plugin;

import android.app.Activity;
import android.util.Log;

import com.chartboost.sdk.Chartboost;
import com.chartboost.sdk.Chartboost.CBFramework;
import com.chartboost.sdk.ChartboostDelegate;
import com.chartboost.sdk.Libraries.CBLogging.Level;
import com.chartboost.sdk.Model.CBError;
import com.meevii.adsdk.base_plugin.adcommon.ADSDKLog;
import com.unity3d.player.UnityPlayer;

import java.util.HashMap;

public class CBPlugin {

    private static HashMap<String, Interstitial> interListeners;
    private static HashMap<String, RewardVideo> rewardListeners;

    private static final String CB_UNITY_SDK_VERSION_STRING = "7.3.0";
    private static CBPlugin _instance;
    public Activity _activity;

    public static CBPlugin instance() {
        if (_instance == null) {
            _instance = new CBPlugin();
        }
        return _instance;
    }

    private Activity getActivity() {
        if (this._activity != null) {
            return this._activity;
        }
        return UnityPlayer.currentActivity;
    }

    public void init(final String appId, final String appSignature, final String unityVersion) {
        this._activity = UnityPlayer.currentActivity;
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                Chartboost.startWithAppId(CBPlugin.this.getActivity(), appId, appSignature);
                Chartboost.setFramework(CBFramework.CBFrameworkUnity, unityVersion);
                Chartboost.setChartboostWrapperVersion(CBPlugin.CB_UNITY_SDK_VERSION_STRING);
                Chartboost.setLoggingLevel(Level.ALL);
                Chartboost.setDelegate(new MyChartboostDelegate());
                Chartboost.onCreate(CBPlugin.this.getActivity());
                Chartboost.onStart(CBPlugin.this.getActivity());
                Log.d("Unity", "CBPlugin: Plugin Initialized");
            }
        });
    }

    class C00023 implements Runnable {
        C00023() {
        }

        public void run() {
            try {
                Chartboost.onStop(CBPlugin.this.getActivity());
                Chartboost.onDestroy(CBPlugin.this.getActivity());
            } catch (Exception ignored) {
            }

        }
    }

    public void setInterListener(String id, Interstitial listener) {
        if (interListeners == null) {
            interListeners = new HashMap<>();
        }
        interListeners.put(id, listener);
    }

    public void setRewardListener(String id, RewardVideo listener) {
        if (rewardListeners == null) {
            rewardListeners = new HashMap<>();
        }
        rewardListeners.put(id, listener);
    }

    public void pause(final boolean pause) {
        ADSDKLog.Log("[Chartboost] pause:" + pause);
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                if (pause) {
                    Chartboost.onPause(CBPlugin.this.getActivity());
                } else {
                    Chartboost.onResume(CBPlugin.this.getActivity());
                }
            }
        });
    }

    public void destroy() {
        ADSDKLog.Log("[Chartboost] destroy");
        getActivity().runOnUiThread(new C00023());
    }

    public class MyChartboostDelegate extends ChartboostDelegate {
        @Override
        public void didCacheInterstitial(String location) {
            ADSDKLog.Log("[Chartboost] Interstitial didCacheInterstitial!");
            if (interListeners != null && interListeners.containsKey(location)) {
                interListeners.get(location).OnAdLoad();
            }
        }

        @Override
        public void didCloseInterstitial(String location) {
            ADSDKLog.Log("[Chartboost] Interstitial didCloseInterstitial!");
            if (interListeners != null && interListeners.containsKey(location)) {
                interListeners.get(location).OnAdClosed();
            }
        }

        @Override
        public void didFailToLoadInterstitial(String location, CBError.CBImpressionError error) {
            ADSDKLog.Log("[Chartboost] Interstitial didFailToLoadInterstitial: " + error);
            if (interListeners != null && interListeners.containsKey(location)) {
                interListeners.get(location).OnAdFailedToLoad("", 0);
            }
        }

        @Override
        public void didClickInterstitial(String location) {
            ADSDKLog.Log("[Chartboost] Interstitial didClickInterstitial!");
            if (interListeners != null && interListeners.containsKey(location)) {
                interListeners.get(location).OnAdClicked();
            }
        }

        @Override
        public void didCacheRewardedVideo(String location) {
            ADSDKLog.Log("[Chartboost] Reward didClickInterstitial!");
            if (rewardListeners != null && rewardListeners.containsKey(location)) {
                rewardListeners.get(location).OnAdLoad();
            }
        }

        @Override
        public void didCloseRewardedVideo(String location) {
            ADSDKLog.Log("[Chartboost] Reward didClickInterstitial!");
            if (rewardListeners != null && rewardListeners.containsKey(location)) {
                rewardListeners.get(location).OnAdClosed();
            }
        }

        @Override
        public void didFailToLoadRewardedVideo(String location, CBError.CBImpressionError error) {
            ADSDKLog.Log("[Chartboost] Reward didClickInterstitial!");
            if (rewardListeners != null && rewardListeners.containsKey(location)) {
                rewardListeners.get(location).OnAdFailedToLoad("", 0);
            }
        }

        @Override
        public void didClickRewardedVideo(String location) {
            ADSDKLog.Log("[Chartboost] Reward didClickInterstitial!");
            if (rewardListeners != null && rewardListeners.containsKey(location)) {
                rewardListeners.get(location).OnAdClicked();
            }
        }

        @Override
        public void didCompleteRewardedVideo(String location, int reward) {
            ADSDKLog.Log("[Chartboost] Reward didClickInterstitial!");
            if (rewardListeners != null && rewardListeners.containsKey(location)) {
                rewardListeners.get(location).isGotReward = true;
            }
        }
    }
}
