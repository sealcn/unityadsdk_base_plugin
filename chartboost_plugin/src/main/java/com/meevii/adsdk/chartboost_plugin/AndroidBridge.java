package com.meevii.adsdk.chartboost_plugin;

import android.app.Activity;
import android.content.Context;

import com.meevii.adsdk.base_plugin.adcommon.ADSDKLog;
import com.meevii.adsdk.base_plugin.adcommon.BaseBridge;
import com.meevii.adsdk.base_plugin.adcommon.IAdListener;

public class AndroidBridge extends BaseBridge {

    private static AdObjectFactory objFactorys;

    public static String CreateInstance(Activity activity, String type, String adUintId, IAdListener listener) {
        if (objFactorys == null) {
            objFactorys = new AdObjectFactory();
        }

        return objFactorys.createInstance(activity, type, adUintId, listener);
    }

    public static void SetSdkKey(Activity activity, String appId, String appSign, String unityVersion) {
        ADSDKLog.Log("[Chartboost] SetSdkKey: appId:" + appId + ", appSign:" + appSign + ", unityVersion:" + unityVersion);
        CBPlugin.instance().init(appId, appSign, unityVersion);
    }

    public static Object Invoke(Context context, String className, String objUUId, String methodName) {
        if (objFactorys == null) {
            return null;
        }
        Object obj = objFactorys.getObject(objUUId);
        if (obj == null) {
            return null;
        }
        return BaseBridge.InvokeMethod(obj, context, className, methodName);
    }
}
