package com.meevii.adsdk.chartboost_plugin;

import android.content.Context;

import com.chartboost.sdk.Chartboost;
import com.meevii.adsdk.base_plugin.adcommon.ADSDKLog;
import com.meevii.adsdk.base_plugin.adcommon.IAdListener;
import com.meevii.adsdk.base_plugin.adcommon.ThreadHandler;

public class Interstitial {

    private String mAdUnitId;
    private IAdListener adListener;

    public Interstitial(final Context context, final String adUnitId) {
        ADSDKLog.Log("[Chartboost] Interstitial!");
        mAdUnitId = adUnitId;
        CBPlugin.instance().setInterListener(mAdUnitId, this);
    }

    public void setAdListener(IAdListener adListener) {
        this.adListener = adListener;
    }

    public void loadAd(Context context) {
        ADSDKLog.Log("[Chartboost] loadAd!");
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                Chartboost.cacheInterstitial(mAdUnitId);
            }
        });
    }

    public void OnAdLoad() {
        ADSDKLog.Log("[Chartboost] OnAdLoad!");
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdLoaded();
                    }
                }
            }).start();
        }
    }

    public void OnAdFailedToLoad(final String errorMessage, final int errorCode) {
        ADSDKLog.Log("[Chartboost] OnAdFailedToLoad!");
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdFailedToLoad(errorMessage, errorCode);
                    }
                }
            }).start();
        }
    }

    public void OnAdClicked() {
        ADSDKLog.Log("[Chartboost] OnAdClicked!");
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdClick();
                    }
                }
            }).start();
        }
    }

    public void OnAdClosed() {
        ADSDKLog.Log("[Chartboost] OnAdClosed!");
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdClosed();
                    }
                }
            }).start();
        }
    }

    public void show(final Context context) {
        ADSDKLog.Log("[Chartboost] Show!");
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                if (isValid(context)) {
                    Chartboost.showInterstitial(mAdUnitId);
                }
            }
        });
    }

    public void destroy(Context context) {
        this.adListener = null;
    }

    public boolean isValid(Context context) {
        ADSDKLog.Log("[Chartboost] isValid!");
        return Chartboost.hasInterstitial(mAdUnitId);
    }
}
