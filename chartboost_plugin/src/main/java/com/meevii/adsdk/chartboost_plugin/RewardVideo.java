package com.meevii.adsdk.chartboost_plugin;

import android.app.Activity;
import android.content.Context;

import com.chartboost.sdk.Chartboost;
import com.meevii.adsdk.base_plugin.adcommon.IAdListener;
import com.meevii.adsdk.base_plugin.adcommon.IRewardAdListener;
import com.meevii.adsdk.base_plugin.adcommon.ThreadHandler;

public class RewardVideo {
    private String mAdUnitId;
    private IAdListener adListener;

    public boolean isGotReward = false;

    public RewardVideo(final Activity unityActivity, final String adUnitId) {
        mAdUnitId = adUnitId;
        CBPlugin.instance().setRewardListener(adUnitId, this);
    }

    public void setAdListener(IAdListener adListener) {
        this.adListener = adListener;
    }

    public void OnAdLoad() {
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdLoaded();
                    }
                }
            }).start();
        }
    }

    public void OnAdFailedToLoad(final String errorMessage, final int errorCode) {
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdFailedToLoad(errorMessage, errorCode);
                    }
                }
            }).start();
        }
    }

    public void OnAdClosed() {
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        if (adListener instanceof IRewardAdListener) {
                            if (isGotReward) {
                                ((IRewardAdListener) adListener).onAdReward();
                            } else {
                                ((IRewardAdListener) adListener).onAdNotReward();
                            }
                        }
                        isGotReward = false;
                        adListener.onAdClosed();
                    }
                }
            }).start();
        }
    }

    public void OnAdClicked() {
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    adListener.onAdClick();
                }
            }).start();
        }
    }

    public void loadAd(Context context) {
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                Chartboost.cacheRewardedVideo(mAdUnitId);
            }
        });
    }

    public void show(final Context context) {
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                if (isValid(context)) {
                    Chartboost.showRewardedVideo(mAdUnitId);
                }
            }
        });
    }

    public void destroy(Context context) {
        this.adListener = null;
    }

    public boolean isValid(Context context) {
        return Chartboost.hasRewardedVideo(mAdUnitId);
    }
}