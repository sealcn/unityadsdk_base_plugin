package com.meevii.adsdk.base_plugin.adcommon;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnLayoutChangeListener;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.WindowManager;
import android.widget.PopupWindow;

public abstract class AdBanner {

    protected View mAdView;

    private Context mContext;

    private PopupWindow mPopupWindow;
    private int mPositionCode = AdPosition.Bottom;
    private boolean mHidden;
    private boolean mDestroy;
    private IAdListener mUnityListener;
    private AdBannerSize mAdSize;
    private OnLayoutChangeListener mLayoutChangeListener;
    private OnGlobalLayoutListener mViewTreeLayoutChangeListener;

    public AdBanner(Context context, AdBannerSize adSize) {
        mContext = context;
        mAdSize = adSize;
    }

    public void setAdListener(IAdListener listener) {
        mUnityListener = listener;
    }

    public void create(final String adUnitId) {
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                createAdView(adUnitId, mAdSize);
                mHidden = false;
            }
        });
    }

    protected abstract View CreateInstance(Context context, String adId, AdBannerSize adSize);

    private void createAdView(String adUnitId, AdBannerSize adSize) {
        this.mAdView = CreateInstance(mContext, adUnitId, AdBannerSize.BANNER);
        this.registerEvent();
    }

    protected abstract void setListener(View adView);

    protected void registerEvent() {
        setListener(mAdView);
    }

    protected void onAdLoaded() {
        if (mPopupWindow == null || !this.mPopupWindow.isShowing()) {
            if (mUnityListener != null) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        if (mUnityListener != null) {
                            mUnityListener.onAdLoaded();
                        }
                    }
                }).start();
            }
        }
    }

    protected void onAdFailedToLoad(final String strErr, final int rawErrorCode) {
        if (mUnityListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (mUnityListener != null) {
                        mUnityListener.onAdFailedToLoad(strErr, rawErrorCode);
                    }
                }
            }).start();
        }
    }

    protected void onAdClick() {
        if (mUnityListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (mUnityListener != null) {
                        mUnityListener.onAdClick();
                    }
                }
            }).start();
        }
    }

    protected void onAdOpened() {
        if (mUnityListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (mUnityListener != null) {
                        mUnityListener.onAdOpened();
                    }
                }
            }).start();
        }
    }

    protected void onAdClosed() {
        if (mUnityListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (mUnityListener != null) {
                        mUnityListener.onAdClosed();
                    }
                }
            }).start();
        }
    }

    protected void onAdLeftApplication() {
        if (mUnityListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (mUnityListener != null) {
                        mUnityListener.onAdLeftApplication();
                    }
                }
            }).start();
        }
    }

    private void createPopupWindow(final Activity activity) {
        if (mPopupWindow == null) {
            this.mLayoutChangeListener = new OnLayoutChangeListener() {
                public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                    if (mPopupWindow.isShowing() && !mHidden) {
                        updatePosition(activity);
                    }
                }
            };

            //activity.getWindow().getDecorView().getRootView().addOnLayoutChangeListener(this.mLayoutChangeListener);
            this.mViewTreeLayoutChangeListener = new OnGlobalLayoutListener() {
                public void onGlobalLayout() {
                    ADSDKLog.Log("[dxy]mViewTreeLayoutChangeListener.................");
                    //updatePosition();
                }
            };

            //this.mAdView.getViewTreeObserver().addOnGlobalLayoutListener(this.mViewTreeLayoutChangeListener);

            //int popUpWindowWidth = mAdSize.equals(AdBannerSize.SMART_BANNER)
            //        ? ViewGroup.LayoutParams.MATCH_PARENT
            //        : mAdSize.getWidthInPixels(activity);

            int popUpWindowWidth = ViewGroup.LayoutParams.MATCH_PARENT;
            if (isUpdateWidth()) {
                popUpWindowWidth = ViewGroup.LayoutParams.WRAP_CONTENT;
            }
            int popUpWindowHeight = mAdSize.getHeightInPixels(activity);
            this.mPopupWindow = new PopupWindow(this.mAdView, popUpWindowWidth, popUpWindowHeight);
            int visibilityFlags = activity.getWindow().getAttributes().flags;
            this.mPopupWindow.getContentView().setSystemUiVisibility(visibilityFlags);

            PluginUtils.setPopUpWindowLayoutType(this.mPopupWindow, WindowManager.LayoutParams.TYPE_APPLICATION_SUB_PANEL);
        }

    }

    private void showPopUpWindow(Activity activity) {
        View anchorView = activity.getWindow().getDecorView().getRootView();
        mPopupWindow.showAtLocation(anchorView, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
    }

    protected abstract void doLoadAd(View adView);

    public void loadAd(Context context) {
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                if (mAdView != null) {
                    doLoadAd(mAdView);
                }
            }
        });
    }

    public boolean isUpdateWidth() {
        return false;
    }

    protected abstract void resumeAdView(View adView);

    protected abstract void pauseAdView(View adView);

    public void show(Context context) {
        if (!(context instanceof Activity)) {
            ADSDKLog.Log("error: banner show must use Activity context!");
            return;
        }
        show((Activity) context);
    }

    public void show(final Activity activity) {
        activity.runOnUiThread(new Runnable() {
            public void run() {
                if (mAdView != null) {
                    if (isCustomShow()) {
                        customShow(activity);
                    } else {
                        createPopupWindow(activity);
                        mHidden = false;
                        mAdView.setVisibility(View.VISIBLE);
                        mPopupWindow.setTouchable(true);
                        mPopupWindow.update();
                        if (!mPopupWindow.isShowing()) {
                            showPopUpWindow(activity);
                        }
                    }
                    resumeAdView(mAdView);
                }
            }
        });
    }

    protected boolean isCustomShow(){
        return false;
    }

    protected void customShow(Activity activity){

    }

    public void hide(Context context) {

        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                mHidden = true;
                destoryWindow();
                if (mAdView != null) {
                    pauseAdView(mAdView);
                }
            }
        });
    }

    protected abstract void doDestroy(View adView);

    private void hideWindow() {
        if (mAdView != null)
            mAdView.setVisibility(View.GONE);

        if (mPopupWindow != null) {
            mPopupWindow.setTouchable(false);
            mPopupWindow.update();
        }
    }

    private void destoryWindow() {
        if (mAdView != null) {
            mAdView.setVisibility(View.GONE);
        }

        if (mPopupWindow != null) {
            ViewParent parentView = mAdView.getParent();
            if (parentView != null && parentView instanceof ViewGroup) {
                ((ViewGroup) parentView).removeView(mAdView);
            }

            mPopupWindow.setTouchable(false);
            mPopupWindow.dismiss();
            mPopupWindow = null;
        }
    }

    public void destroy(Context context) {
        if (!mDestroy) {
            mDestroy = true;
            ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
                @Override
                public void onHandler() {
                    if (mAdView != null) {
                        destoryWindow();
                        doDestroy(mAdView);
                    }
                }
            });
        }
    }

    public void setPosition(final Activity activity, final int positionX, final int positionY) {
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                updatePosition(activity);
            }
        });
    }

    private void updatePosition(Activity activity) {
        if (this.mPopupWindow != null && this.mPopupWindow.isShowing()) {
            View anchorView = activity.getWindow().getDecorView().getRootView();
            mPopupWindow.showAtLocation(anchorView, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0); //有些机器此方法会不生效(Moto Z Droid, android 7.0)
        }
    }

    private Point getPositionInPixels(View anchorView) {
        int adViewWidth = this.mAdSize.getWidthInPixels(mContext);
        int adViewHeight = this.mAdSize.getHeightInPixels(mContext);
        ADSDKLog.Log("anchorView ( " + anchorView.getWidth() + " * " + anchorView.getHeight() + " )");
        ADSDKLog.Log("adView ( " + adViewWidth + " * " + adViewHeight + " )");

        int x = PluginUtils.getHorizontalOffsetForPositionCode(mPositionCode, adViewWidth, anchorView.getWidth());
        int y = PluginUtils.getVerticalOffsetForPositionCode(mPositionCode, adViewHeight, anchorView.getHeight());
        ADSDKLog.Log("getPositionInPixels point : ( " + x + " * " + y + " )");

        return new Point(x, y);
    }
}
