package com.meevii.adsdk.base_plugin.adcommon;

import android.content.Context;

import java.lang.reflect.Method;

public class BaseBridge {
    //invoke
    public static Object InvokeMethod(Object obj, Context context, String className, String methodName) {
        Class<?> clz;
        try {
            clz = Class.forName(className);
            Method m = clz.getMethod(methodName, Context.class);
            return m.invoke(obj, context);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean toBool(Object obj) {
        if (obj == null)
            return false;
        return (Boolean) obj;
    }

    public static String toString(Object obj) {
        if (obj == null)
            return "";
        return (String) obj;
    }

    public static int getApiVersion() {
        return android.os.Build.VERSION.SDK_INT;
    }
}
