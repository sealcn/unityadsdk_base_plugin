package com.meevii.adsdk.base_plugin.adcommon;

public interface IAdListener {
    void onAdLoaded();

    void onAdFailedToLoad(String errorReason, int rawErrorCode);

    void onAdClick();

    void onAdOpened();

    void onAdClosed();

    void onAdLeftApplication();
}
