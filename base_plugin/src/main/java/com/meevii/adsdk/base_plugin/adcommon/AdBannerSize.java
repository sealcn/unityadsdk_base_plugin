package com.meevii.adsdk.base_plugin.adcommon;


import android.content.Context;
import android.util.DisplayMetrics;
import android.util.TypedValue;

public final class AdBannerSize {
    public static final int FULL_WIDTH = -1;
    public static final int AUTO_HEIGHT = -2;
    public static final AdBannerSize BANNER = new AdBannerSize(FULL_WIDTH, 50, "320x50_mb");
    public static final AdBannerSize SMART_BANNER = new AdBannerSize(FULL_WIDTH, AUTO_HEIGHT, "smart_banner");
    private final int width;
    private final int height;
    private final String name;

    public AdBannerSize(int width, int height) {
        this.width = width;
        this.height = height;
        String xDescribe = width == FULL_WIDTH ? "FULL" : String.valueOf(width);
        String yDescribe = height == AUTO_HEIGHT ? "AUTO" : String.valueOf(height);
        this.name = xDescribe + "x" + yDescribe;
    }

    public AdBannerSize(int width, int height, String name) {
        this.width = width;
        this.height = height;
        this.name = name;
    }

    public final int getHeight() {
        return this.height;
    }

    public final int getWidth() {
        return this.width;
    }

    public final boolean equals(AdBannerSize adSize) {
        if (adSize == this) {
            return true;
        }

        return this.width == adSize.width && this.height == adSize.height && this.name.equals(adSize.name);
    }

    private static int calcuFitHeight(DisplayMetrics dm) {
        int height = (int)((float)dm.heightPixels / dm.density);
        if (height <= 400) {
            return 32;
        } else {
            return height <= 720 ? 50 : 90;
        }
    }

    public static int getPxFromDP(DisplayMetrics dm, int dp) {
        return (int)TypedValue.applyDimension(1, (float)dp, dm);
    }

    public final int getHeightInPixels(Context context) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        switch(this.height) {
            case AUTO_HEIGHT:
                return (int)((float)calcuFitHeight(dm) * dm.density);
            default:
                return getPxFromDP(dm, this.height);
        }
    }

    public final int getWidthInPixels(Context context) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        switch(this.width) {
            case FULL_WIDTH:
                return dm.widthPixels;
            default:
                return getPxFromDP(dm, this.width);
        }
    }

    public final boolean isAutoHeight() {
        return this.height == AUTO_HEIGHT;
    }

    public final boolean isFullWidth() {
        return this.width == FULL_WIDTH;
    }

    public final String toString() {
        return this.name;
    }
}
