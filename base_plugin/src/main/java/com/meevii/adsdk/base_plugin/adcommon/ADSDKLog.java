package com.meevii.adsdk.base_plugin.adcommon;

import android.util.Log;

public class ADSDKLog {

    private static boolean isShowLog = false;

    public static final void SetIsShowLog(boolean isShowLog) {
        ADSDKLog.isShowLog = isShowLog;
    }

    public static final void Log(String message) {
        if (isShowLog) {
            Log.d("[Unity ADSDK Android]", message);
        }
    }

    public static boolean isShowLog(){
        return isShowLog;
    }

}
