package com.meevii.adsdk.base_plugin.adcommon;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

public class ThreadHandler {
    public interface IHander{
        void onHandler();
    }

    public static void executeOnMainThread(final IHander h){

        Observable.just("")
                .observeOn(AndroidSchedulers.mainThread(), false, 100)
                .subscribe(new Observer<String>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        //do nothing
                    }

                    @Override
                    public void onNext(String o) {
                        h.onHandler();
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                        //do nothing
                    }
                });
    }
}
