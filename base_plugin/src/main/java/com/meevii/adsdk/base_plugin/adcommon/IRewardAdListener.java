package com.meevii.adsdk.base_plugin.adcommon;

public interface IRewardAdListener extends IAdListener {
    void onAdReward();

    void onAdNotReward();
}
