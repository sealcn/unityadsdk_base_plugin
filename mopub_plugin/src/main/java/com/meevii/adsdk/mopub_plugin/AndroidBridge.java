package com.meevii.adsdk.mopub_plugin;

import android.app.Activity;
import android.content.Context;

import com.meevii.adsdk.base_plugin.adcommon.ADSDKLog;
import com.meevii.adsdk.base_plugin.adcommon.BaseBridge;
import com.meevii.adsdk.base_plugin.adcommon.IAdListener;
import com.meevii.adsdk.base_plugin.adcommon.ThreadHandler;
import com.mopub.common.MoPub;
import com.mopub.common.SdkConfiguration;
import com.mopub.common.SdkInitializationListener;

import static com.mopub.common.logging.MoPubLog.LogLevel.DEBUG;
import static com.mopub.common.logging.MoPubLog.LogLevel.NONE;

public class AndroidBridge extends BaseBridge {

    private static AdObjectFactory objFactorys;
    public static boolean isInit;

    public static String CreateInstance(Activity activity, String type, String adUintId, IAdListener listener) {
        if (objFactorys == null) {
            objFactorys = new AdObjectFactory();
        }

        return objFactorys.createInstance(activity, type, adUintId, listener);
    }

    public static Object Invoke(Context context, String className, String objUUId, String methodName) {
        if (objFactorys == null)
            return null;
        Object obj = objFactorys.getObject(objUUId);
        if (obj == null) {
            return null;
        }
        return BaseBridge.InvokeMethod(obj, context, className, methodName);
    }

    public static void InitWithAppId(final Context context, final String appId) {
        ADSDKLog.Log("InitWithAppId:" + appId);
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                final SdkConfiguration.Builder configBuilder = new SdkConfiguration.Builder(appId);

                if (ADSDKLog.isShowLog()) {
                    configBuilder.withLogLevel(DEBUG);
                } else {
                    configBuilder.withLogLevel(NONE);
                }
                MoPub.initializeSdk(context, configBuilder.build(), initSdkListener());
            }
        });

    }

    private static SdkInitializationListener initSdkListener() {
        return new SdkInitializationListener() {

            @Override
            public void onInitializationFinished() {
                isInit = true;

                MoPubUtils.initIfNeed();
            }
        };
    }
}
