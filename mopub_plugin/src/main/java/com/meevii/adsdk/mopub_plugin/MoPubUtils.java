package com.meevii.adsdk.mopub_plugin;

import android.support.annotation.NonNull;

import com.meevii.adsdk.base_plugin.adcommon.ThreadHandler;
import com.mopub.common.MoPubReward;
import com.mopub.mobileads.MoPubErrorCode;
import com.mopub.mobileads.MoPubRewardedVideoListener;
import com.mopub.mobileads.MoPubRewardedVideos;

import java.util.HashMap;
import java.util.Set;

public class MoPubUtils {
    private static boolean isNeedInit = true;
    private static HashMap<String, MoPubRewardedVideoListener> listeners = new HashMap<>();

    public static String getErrorReason(int errorCode) {
        return "" + errorCode;
    }

    public static void setListener(final String adUnitId, MoPubRewardedVideoListener adListener) {
        if (listeners == null) {
            listeners = new HashMap<>();
        }

        listeners.remove(adUnitId);
        if (adListener != null) {
            listeners.put(adUnitId, adListener);
        }
        final MoPubRewardedVideoListener listener = getAdListener(adUnitId);
        if (listener != null && MoPubRewardedVideos.hasRewardedVideo(adUnitId)) {
            ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
                @Override
                public void onHandler() {
                    listener.onRewardedVideoLoadSuccess(adUnitId);
                }
            });
        }
    }

    public static void initIfNeed() {
        if (isNeedInit) {
            isNeedInit = false;
            MoPubRewardedVideos.setRewardedVideoListener(new MoPubRewardedVideoListener() {
                @Override
                public void onRewardedVideoLoadSuccess(@NonNull String adUnitId) {
                    MoPubRewardedVideoListener listener = getAdListener(adUnitId);
                    if (listener != null) {
                        listener.onRewardedVideoLoadSuccess(adUnitId);
                    }
                }

                @Override
                public void onRewardedVideoLoadFailure(@NonNull String adUnitId, @NonNull MoPubErrorCode errorCode) {
                    MoPubRewardedVideoListener listener = getAdListener(adUnitId);
                    if (listener != null) {
                        listener.onRewardedVideoLoadFailure(adUnitId, errorCode);
                    }
                }

                @Override
                public void onRewardedVideoStarted(@NonNull String adUnitId) {
                    MoPubRewardedVideoListener listener = getAdListener(adUnitId);
                    if (listener != null) {
                        listener.onRewardedVideoStarted(adUnitId);
                    }
                }

                @Override
                public void onRewardedVideoPlaybackError(@NonNull String adUnitId, @NonNull MoPubErrorCode errorCode) {
                    MoPubRewardedVideoListener listener = getAdListener(adUnitId);
                    if (listener != null) {
                        listener.onRewardedVideoPlaybackError(adUnitId, errorCode);
                    }
                }

                @Override
                public void onRewardedVideoClicked(@NonNull String adUnitId) {
                    MoPubRewardedVideoListener listener = getAdListener(adUnitId);
                    if (listener != null) {
                        listener.onRewardedVideoClicked(adUnitId);
                    }
                }

                @Override
                public void onRewardedVideoClosed(@NonNull String adUnitId) {
                    MoPubRewardedVideoListener listener = getAdListener(adUnitId);
                    if (listener != null) {
                        listener.onRewardedVideoClosed(adUnitId);
                    }
                }

                @Override
                public void onRewardedVideoCompleted(@NonNull Set<String> adUnitIds, @NonNull MoPubReward reward) {
                    MoPubRewardedVideoListener listener = getAdListener(adUnitIds);
                    if (listener != null) {
                        listener.onRewardedVideoCompleted(adUnitIds, reward);
                    }
                }
            });
        }
    }

    private static MoPubRewardedVideoListener getAdListener(String adUnitId) {
        if (listeners == null) {
            return null;
        }
        return listeners.get(adUnitId);
    }

    private static MoPubRewardedVideoListener getAdListener(Set<String> adUnitIds) {
        if (listeners == null || adUnitIds == null) {
            return null;
        }

        String[] list = new String[10];
        list = adUnitIds.toArray(list);

        for (String s : list) {
            MoPubRewardedVideoListener listener = listeners.get(s);
            if (listener != null) {
                return listener;
            }
        }

        return null;
    }

}
