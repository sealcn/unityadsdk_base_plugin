package com.meevii.adsdk.mopub_plugin;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;

import com.meevii.adsdk.base_plugin.adcommon.IAdListener;
import com.meevii.adsdk.base_plugin.adcommon.IRewardAdListener;
import com.meevii.adsdk.base_plugin.adcommon.ThreadHandler;
import com.mopub.common.MoPubReward;
import com.mopub.mobileads.MoPubErrorCode;
import com.mopub.mobileads.MoPubRewardedVideoListener;
import com.mopub.mobileads.MoPubRewardedVideos;

import java.util.Set;

public class RewardVideo {
    private IAdListener mAdListener;
    private String mAdUnitId;

    private boolean isGotReward = false;

    public RewardVideo(final Activity unityActivity, final String adUnitId, IAdListener adListener) {
        mAdUnitId = adUnitId;
        mAdListener = adListener;
    }

    public void loadAd(Context context) {
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                MoPubUtils.setListener(mAdUnitId, getListener());
                MoPubRewardedVideos.loadRewardedVideo(mAdUnitId);
            }
        });
    }

    private MoPubRewardedVideoListener getListener() {
        return new MoPubRewardedVideoListener() {
            @Override
            public void onRewardedVideoLoadSuccess(@NonNull String adUnitId) {
                OnAdLoad();
            }

            @Override
            public void onRewardedVideoLoadFailure(@NonNull String adUnitId, @NonNull MoPubErrorCode errorCode) {
                OnAdFailedToLoad(errorCode.toString(), errorCode.getIntCode());
            }

            @Override
            public void onRewardedVideoStarted(@NonNull String adUnitId) {
                OnAdOpened();
            }

            @Override
            public void onRewardedVideoPlaybackError(@NonNull String adUnitId, @NonNull MoPubErrorCode errorCode) {
            }

            @Override
            public void onRewardedVideoClicked(@NonNull String adUnitId) {
                OnAdClicked();
            }

            @Override
            public void onRewardedVideoClosed(@NonNull String adUnitId) {
                OnAdClosed();
            }

            @Override
            public void onRewardedVideoCompleted(@NonNull Set<String> adUnitIds, @NonNull MoPubReward reward) {
                isGotReward = true;
            }
        };
    }

    public void show(Context context) {
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                MoPubRewardedVideos.showRewardedVideo(mAdUnitId);
            }
        });
    }

    private void OnAdLoad() {
        if (mAdListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (mAdListener != null) {
                        mAdListener.onAdLoaded();
                    }
                }
            }).start();
        }
    }

    private void OnAdFailedToLoad(final String errorMessage, final int errorCode) {
        if (mAdListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (mAdListener != null) {
                        mAdListener.onAdFailedToLoad(errorMessage, errorCode);
                    }
                }
            }).start();
        }
    }

    private void OnAdClicked() {
        if (mAdListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (mAdListener != null) {
                        mAdListener.onAdClick();
                    }
                }
            }).start();
        }
    }

    private void OnAdClosed() {
        if (mAdListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (mAdListener != null) {
                        if (mAdListener instanceof IRewardAdListener) {
                            if (isGotReward) {
                                ((IRewardAdListener) mAdListener).onAdReward();
                            } else {
                                ((IRewardAdListener) mAdListener).onAdNotReward();
                            }
                        }
                        isGotReward = false;
                        mAdListener.onAdClosed();
                    }
                }
            }).start();
        }
    }

    private void OnAdOpened() {
        if (mAdListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (mAdListener != null) {
                        mAdListener.onAdOpened();
                    }
                }
            }).start();
        }
    }

    public void destroy(Context context) {
        this.mAdListener = null;
    }

}