package com.meevii.adsdk.mopub_plugin;

import android.content.Context;
import android.view.View;

import com.meevii.adsdk.base_plugin.adcommon.ADSDKLog;
import com.meevii.adsdk.base_plugin.adcommon.AdBanner;
import com.meevii.adsdk.base_plugin.adcommon.AdBannerSize;
import com.mopub.mobileads.MoPubErrorCode;
import com.mopub.mobileads.MoPubView;

public class Banner extends AdBanner {

    private MoPubView mMoPubView;

    private Banner(Context context, AdBannerSize adSize) {
        super(context, adSize);
    }

    public Banner(Context applicationContext, String adUnitId, AdBannerSize adSize) {
        super(applicationContext, adSize);
        create(adUnitId);
    }

    @Override
    protected View CreateInstance(Context context, String adId, AdBannerSize adSize) {
        mMoPubView = new MoPubView(context);
        mMoPubView.setAdUnitId(adId);
        mMoPubView.setAutorefreshEnabled(false);
        mMoPubView.setBannerAdListener(new MoPubView.BannerAdListener() {
            @Override
            public void onBannerLoaded(MoPubView banner) {
                onAdLoaded();
            }

            @Override
            public void onBannerFailed(MoPubView banner, MoPubErrorCode errorCode) {
                if (errorCode != null) {
                    onAdFailedToLoad(errorCode.toString(), errorCode.getIntCode());
                } else {
                    onAdFailedToLoad("", -1);
                }
            }

            @Override
            public void onBannerClicked(MoPubView banner) {
                onAdClick();
            }

            @Override
            public void onBannerExpanded(MoPubView banner) {
                ADSDKLog.Log("[MoPub] Expanded");
            }

            @Override
            public void onBannerCollapsed(MoPubView banner) {
                ADSDKLog.Log("[MoPub] Collapsed");
            }
        });

        return mMoPubView;
    }

    @Override
    protected void setListener(View adView) {
    }

    @Override
    protected void doLoadAd(View adView) {
        if (mMoPubView != null) {
            mMoPubView.loadAd();
        }
    }

    @Override
    protected void doDestroy(View adView) {
        if (mMoPubView != null) {
            mMoPubView.destroy();
        }
        mMoPubView = null;
    }

    @Override
    protected void resumeAdView(View adView) {
    }

    @Override
    protected void pauseAdView(View adView) {
    }
}
