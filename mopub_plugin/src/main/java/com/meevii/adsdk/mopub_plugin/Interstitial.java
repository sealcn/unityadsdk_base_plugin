package com.meevii.adsdk.mopub_plugin;

import android.app.Activity;
import android.content.Context;

import com.meevii.adsdk.base_plugin.adcommon.ADSDKLog;
import com.meevii.adsdk.base_plugin.adcommon.IAdListener;
import com.meevii.adsdk.base_plugin.adcommon.ThreadHandler;
import com.mopub.mobileads.MoPubErrorCode;
import com.mopub.mobileads.MoPubInterstitial;

public class Interstitial {

    private MoPubInterstitial mMoPubInterstitial;
    private IAdListener adListener;

    public Interstitial(final Context context, final String adUnitId) {
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                mMoPubInterstitial = new MoPubInterstitial((Activity) context, adUnitId);
                registerEvent();
            }
        });
    }

    public void setAdListener(IAdListener adListener) {
        this.adListener = adListener;
    }

    private void registerEvent() {
        mMoPubInterstitial.setInterstitialAdListener(new MoPubInterstitial.InterstitialAdListener() {
            @Override
            public void onInterstitialLoaded(MoPubInterstitial interstitial) {
                OnAdLoad();
            }

            @Override
            public void onInterstitialFailed(MoPubInterstitial interstitial, MoPubErrorCode errorCode) {
                if (errorCode != null) {
                    OnAdFailedToLoad(errorCode.toString(), errorCode.getIntCode());
                }
            }

            @Override
            public void onInterstitialShown(MoPubInterstitial interstitial) {
                OnAdShown();
            }

            @Override
            public void onInterstitialClicked(MoPubInterstitial interstitial) {
                OnAdClicked();
                OnAdLeftApplication();
            }

            @Override
            public void onInterstitialDismissed(MoPubInterstitial interstitial) {
                OnAdClosed();
            }
        });
    }

    public void loadAd(Context context) {
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                if (mMoPubInterstitial != null) {
                    mMoPubInterstitial.load();
                }
            }
        });
    }

    private void OnAdLoad() {
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdLoaded();
                    }
                }
            }).start();
        }
    }

    private void OnAdFailedToLoad(final String errorMessage, final int errorCode) {
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdFailedToLoad(errorMessage, errorCode);
                    }
                }
            }).start();
        }
    }

    private void OnAdShown() {
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdOpened();
                    }
                }
            }).start();
        }
    }

    private void OnAdClicked() {
        ADSDKLog.Log("[MoPub] OnAdClicked!");
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdClick();
                    }
                }
            }).start();
        }
    }

    private void OnAdClosed() {
        ADSDKLog.Log("[MoPub] OnAdClosed!");
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdClosed();
                    }
                }
            }).start();
        }
    }

    private void OnAdLeftApplication() {
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdLeftApplication();
                    }
                }
            }).start();
        }
    }

    public void show(Context context) {
        ADSDKLog.Log("[MoPub] show");
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                if (mMoPubInterstitial != null) {
                    mMoPubInterstitial.show();
                }
            }
        });
    }

    public void destroy(Context context) {
        if (mMoPubInterstitial != null) {
            mMoPubInterstitial.destroy();
        }
        this.adListener = null;
    }
}
