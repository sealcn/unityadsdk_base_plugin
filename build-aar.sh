#!/usr/bin/env bash
java8
./gradlew build

cp base_plugin/build/outputs/aar/ad-base-plugin.aar ~/U3dProjects/unityadsdk_testtool/Assets/ADSDK/Plugins/Android/ad-base-plugin.aar
cp splash_plugin/build/outputs/aar/splash-plugin.aar ~/U3dProjects/unityadsdk_testtool/Assets/ADSDK/Plugins/Android/splash-plugin.aar

cp adcolony_plugin/build/outputs/aar/adcolony-plugin.aar ~/U3dProjects/unityadsdk_testtool/Assets/ADSDK/AdPlatform/AdColony/Plugins/Android/adcolony-plugin.aar

cp admob_plugin/build/outputs/aar/admob-plugin.aar ~/U3dProjects/unityadsdk_testtool/Assets/ADSDK/AdPlatform/Admob/Plugins/Android/admob-plugin.aar

cp applovin_plugin/build/outputs/aar/applovin-plugin.aar ~/U3dProjects/unityadsdk_testtool/Assets/ADSDK/AdPlatform/AppLovin/Plugins/Android/applovin-plugin.aar

cp buad_plugin/build/outputs/aar/buad-plugin.aar ~/U3dProjects/unityadsdk_testtool/Assets/ADSDK/AdPlatform/BuAd/Plugins/Android/buad-plugin.aar
cp buad_plugin/libs/open_ad_sdk-2.1.0.3.aar ~/U3dProjects/unityadsdk_testtool/Assets/ADSDK/AdPlatform/BuAd/Plugins/Android/open_ad_sdk-2.1.0.3.aar

cp chartboost_plugin/build/outputs/aar/chartboost-plugin.aar ~/U3dProjects/unityadsdk_testtool/Assets/ADSDK/AdPlatform/Chartboost/Plugins/Android/chartboost-plugin.aar

cp facebook_plugin/build/outputs/aar/facebook-plugin.aar ~/U3dProjects/unityadsdk_testtool/Assets/ADSDK/AdPlatform/Facebook/Plugins/Android/facebook-plugin.aar

cp gdt_plugin/build/outputs/aar/gdt-plugin.aar ~/U3dProjects/unityadsdk_testtool/Assets/ADSDK/AdPlatform/GDT/Plugins/Android/gdt-plugin.aar

cp ironsource_plugin/build/outputs/aar/ironsource-plugin.aar ~/U3dProjects/unityadsdk_testtool/Assets/ADSDK/AdPlatform/IronSource/Plugins/Andriod/ironsource-plugin.aar

cp unityads_plugin/build/outputs/aar/unityads-plugin.aar ~/U3dProjects/unityadsdk_testtool/Assets/ADSDK/AdPlatform/UnityAds/Plugins/Android/unityads-plugin.aar
cp unityads_plugin/libs/unity-ads-3.0.3.aar ~/U3dProjects/unityadsdk_testtool/Assets/ADSDK/AdPlatform/UnityAds/Plugins/Android/unity-ads-3.0.3.aar

cp vungle_plugin/build/outputs/aar/vungle-plugin.aar ~/U3dProjects/unityadsdk_testtool/Assets/ADSDK/AdPlatform/Vungle/Plugins/Android/vungle-plugin.aar

cp mopub_plugin/build/outputs/aar/mopub-plugin.aar ~/U3dProjects/unityadsdk_testtool/Assets/ADSDK/AdPlatform/MoPub/Plugins/Android/mopub-plugin.aar