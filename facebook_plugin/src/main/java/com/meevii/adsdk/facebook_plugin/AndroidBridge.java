package com.meevii.adsdk.facebook_plugin;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.meevii.adsdk.base_plugin.adcommon.BaseBridge;
import com.meevii.adsdk.base_plugin.adcommon.IAdListener;

import java.util.UUID;

public class AndroidBridge extends BaseBridge {

    private static AdObjectFactory objFactorys;

    public static String getTestDeviceID(Context ctx) {
        SharedPreferences sp = ctx.getSharedPreferences("FBAdPrefs", 0);
        String deviceID = sp.getString("deviceIdHash", (String) null);
        if (TextUtils.isEmpty(deviceID)) {
            deviceID = UUID.randomUUID().toString();
            sp.edit().putString("deviceIdHash", deviceID).apply();
        }

        return deviceID;
    }

    public static void addTestDevice(Context ctx, String strDeviceID) {
        if (TextUtils.isEmpty(strDeviceID))
            strDeviceID = getTestDeviceID(ctx);
        if (!TextUtils.isEmpty(strDeviceID))
            com.facebook.ads.AdSettings.addTestDevice(strDeviceID);
    }

    public static String CreateInstance(Activity activity, String type, String adUintId, IAdListener listener) {
        if (objFactorys == null) {
            objFactorys = new AdObjectFactory();
        }
        return objFactorys.createInstance(activity, type, adUintId, listener);
    }

    public static Object Invoke(Context context, String className, String objUUId, String methodName) {
        if (objFactorys == null) {
            return null;
        }
        Object obj = objFactorys.getObject(objUUId);
        if (obj == null) {
            return null;
        }
        return InvokeMethod(obj, context, className, methodName);
    }
}
