package com.meevii.adsdk.facebook_plugin;

import android.content.Context;
import android.view.View;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;
import com.meevii.adsdk.base_plugin.adcommon.AdBanner;
import com.meevii.adsdk.base_plugin.adcommon.AdBannerSize;

public class Banner extends AdBanner {

    AdView mAdView;

    private Banner(Context context, AdBannerSize adSize) {
        super(context, adSize);
    }

    public Banner(Context applicationContext, String adUnitId, AdBannerSize adSize) {
        super(applicationContext, adSize);
        create(adUnitId);
    }

    @Override
    protected View CreateInstance(Context context, String adId, AdBannerSize adSize) {
        mAdView = new AdView(context, adId, AdSize.fromWidthAndHeight(adSize.getWidth(), adSize.getHeight()));
        return mAdView;
    }

    @Override
    protected void setListener(View adView) {
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onError(Ad ad, AdError adError) {
                Banner.this.onAdFailedToLoad(adError.getErrorMessage(), adError.getErrorCode());
            }

            @Override
            public void onAdLoaded(Ad ad) {
                Banner.this.onAdLoaded();
            }

            @Override
            public void onAdClicked(Ad ad) {
                Banner.this.onAdClick();
            }

            @Override
            public void onLoggingImpression(Ad ad) {
            }

        });
    }

    @Override
    protected void doLoadAd(View adView) {
        DisableAutoRefresh();
        mAdView.loadAd();
    }

    @Override
    protected void doDestroy(View adView) {
        mAdView.setAdListener(null);
        mAdView.destroy();
    }

    @Override
    protected void resumeAdView(View adView) {
    }

    @Override
    protected void pauseAdView(View adView) {
    }

    public void DisableAutoRefresh() {
        if (mAdView != null) {
            mAdView.disableAutoRefresh();
        }
    }
}
