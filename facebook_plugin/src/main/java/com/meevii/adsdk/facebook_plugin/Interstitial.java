package com.meevii.adsdk.facebook_plugin;

import android.content.Context;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.InterstitialAd;
import com.facebook.ads.InterstitialAdListener;
import com.meevii.adsdk.base_plugin.adcommon.ADSDKLog;
import com.meevii.adsdk.base_plugin.adcommon.IAdListener;
import com.meevii.adsdk.base_plugin.adcommon.ThreadHandler;

public class Interstitial {
    private InterstitialAd mInterstitialAd;
    private IAdListener adListener;
    private boolean isLoaded;
    private String mAdUnitId;

    public Interstitial(final Context context, final String adUnitId) {
        mAdUnitId = adUnitId;
        this.isLoaded = false;

        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                mInterstitialAd = new InterstitialAd(context, mAdUnitId);
                registerEvent();
            }
        });
    }

    public void setAdListener(IAdListener adListener) {
        this.adListener = adListener;
    }

    private void registerEvent() {
        mInterstitialAd.setAdListener(new InterstitialAdListener() {

            @Override
            public void onInterstitialDisplayed(Ad ad) {

            }

            @Override
            public void onInterstitialDismissed(Ad ad) {
                InterstitialAdDidClose();
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                InterstitialAdDidFailWithError(adError.getErrorMessage(), adError.getErrorCode());
            }

            @Override
            public void onAdLoaded(Ad ad) {
                InterstitialAdDidLoad();
            }

            @Override
            public void onAdClicked(Ad ad) {
                InterstitialAdDidClick();
            }

            @Override
            public void onLoggingImpression(Ad ad) {

            }
        });
    }

    public void loadAd(Context context) {
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                if (mInterstitialAd != null) {
                    mInterstitialAd.loadAd();
                }
            }
        });
    }

    private void InterstitialAdDidLoad() {
        isLoaded = true;
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdLoaded();
                    }
                }
            }).start();
        }
    }

    private void InterstitialAdDidFailWithError(final String errorMessage, final int errorCode) {
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdFailedToLoad(errorMessage, errorCode);
                    }
                }
            }).start();
        }
    }

    private void InterstitialAdDidClick() {
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdClick();
                    }
                }
            }).start();
        }
    }

    private void InterstitialAdDidClose() {
        ADSDKLog.Log("facebook InterstitialAdDidClose!");
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdClosed();
                    }
                }
            }).start();
        }
    }

    public boolean isLoaded(Context context) {
        return isLoaded;
    }

    public void show(Context context) {
        ADSDKLog.Log("[facebook] show");
        if (!isLoaded(null)) {
            return;
        }
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                if (isLoaded(null)) {
                    mInterstitialAd.show();
                }
            }
        });
    }

    public void destroy(Context context) {
        this.adListener = null;
        isLoaded = false;
    }

    public boolean isValid(Context context) {
        if (mInterstitialAd == null) {
            return false;
        }
        return isLoaded && mInterstitialAd.isAdLoaded();
    }
}
