package com.meevii.adsdk.facebook_plugin;

import android.content.Context;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.RewardedVideoAd;
import com.facebook.ads.RewardedVideoAdListener;
import com.meevii.adsdk.base_plugin.adcommon.IAdListener;
import com.meevii.adsdk.base_plugin.adcommon.IRewardAdListener;
import com.meevii.adsdk.base_plugin.adcommon.ThreadHandler;

public class RewardVideo {

    private RewardedVideoAd mRewardVideoAd;
    private IAdListener adListener;
    private boolean isLoaded;
    private String mAdUnitId;

    private boolean isGotReward = false;

    public RewardVideo(final Context context, final String adUnitId) {
        mAdUnitId = adUnitId;
        this.isLoaded = false;
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                mRewardVideoAd = new RewardedVideoAd(context, mAdUnitId);
                mRewardVideoAd.setAdListener(new RewardedVideoAdListener() {
                    @Override
                    public void onRewardedVideoCompleted() {
                        isGotReward = true;
                    }

                    @Override
                    public void onLoggingImpression(Ad ad) {
                    }

                    @Override
                    public void onRewardedVideoClosed() {
                        RewardedVideoAdDidClose();
                    }

                    @Override
                    public void onError(Ad ad, AdError adError) {
                        RewardedVideoAdDidFailWithError(adError.getErrorMessage(), adError.getErrorCode());
                    }

                    @Override
                    public void onAdLoaded(Ad ad) {
                        RewardedVideoAdDidLoad();
                    }

                    @Override
                    public void onAdClicked(Ad ad) {
                        RewardedVideoAdDidClick();
                    }
                });
            }
        });
    }

    public void setAdListener(IAdListener adListener) {
        this.adListener = adListener;
    }

    private void RewardedVideoAdDidClick() {
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdClick();
                    }
                }
            }).start();
        }
    }

    private void RewardedVideoAdDidLoad() {
        isLoaded = true;
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdLoaded();
                    }
                }
            }).start();
        }
    }

    private void RewardedVideoAdDidFailWithError(final String errorMessage, final int errorCode) {
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdFailedToLoad(errorMessage, errorCode);
                    }
                }
            }).start();
        }
    }

    private void RewardedVideoAdDidClose() {
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        if (adListener instanceof IRewardAdListener) {
                            if (isGotReward) {
                                ((IRewardAdListener) adListener).onAdReward();
                            } else {
                                ((IRewardAdListener) adListener).onAdNotReward();
                            }
                        }
                        isGotReward = false;
                        adListener.onAdClosed();
                    }
                }
            }).start();
        }
    }

    public void loadAd(Context context) {
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                if (mRewardVideoAd != null) {
                    mRewardVideoAd.loadAd();
                }
            }
        });
    }

    public boolean isLoaded(Context context) {
        return isLoaded;
    }

    public void show(Context context) {
        if (!isLoaded(null))
            return;
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                mRewardVideoAd.show();
            }
        });
    }

    public void destroy(Context context) {
        this.adListener = null;
        this.isLoaded = false;
    }

    public boolean isValid(Context context) {
        if (mRewardVideoAd == null)
            return false;
        return isLoaded && mRewardVideoAd.isAdLoaded();
    }
}
