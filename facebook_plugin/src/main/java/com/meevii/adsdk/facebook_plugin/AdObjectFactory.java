package com.meevii.adsdk.facebook_plugin;

import android.app.Activity;

import com.meevii.adsdk.base_plugin.adcommon.AdBannerSize;
import com.meevii.adsdk.base_plugin.adcommon.IAdListener;

import java.util.HashMap;
import java.util.UUID;

public class AdObjectFactory {

    public HashMap<String, Object> objs = new HashMap<>();

    Object getObject(String id) {
        return objs.get(id);
    }

    public String createInstance(Activity activity, String type, String adUintId, IAdListener listener) {

        Object obj = null;
        if (type.equals(Banner.class.getName())) {
            Banner banner = new Banner(activity, adUintId, AdBannerSize.BANNER);
            banner.setAdListener(listener);
            obj = banner;
        } else if (type.equals(Interstitial.class.getName())) {
            Interstitial interstitial = new Interstitial(activity, adUintId);
            interstitial.setAdListener(listener);
            obj = interstitial;
        } else if (type.equals(RewardVideo.class.getName())) {
            RewardVideo rewardVideo = new RewardVideo(activity, adUintId);
            rewardVideo.setAdListener(listener);
            obj = rewardVideo;
        }
        if (obj != null) {
            String uuid = UUID.randomUUID().toString().replaceAll("-", "");
            objs.put(uuid, obj);
            return uuid;
        }
        return "";
    }

}
