package com.meevii.adsdk.gdt_plugin;

import android.app.Activity;
import android.content.Context;

import com.meevii.adsdk.base_plugin.adcommon.ADSDKLog;
import com.meevii.adsdk.base_plugin.adcommon.BaseBridge;
import com.meevii.adsdk.base_plugin.adcommon.IAdListener;

public class AndroidBridge extends BaseBridge {

    private static AdObjectFactory objFactorys;
    private static String sdkKey;

    public static void SetSdkKey(String key) {
        sdkKey = key;
        ADSDKLog.Log("[GdtAds] In SetSdkKey: sdkKey: " + sdkKey);
    }

    public static void InitializeSdk(Activity currentActivity) {
        ADSDKLog.Log("[GdtAds] In InitializeSdk");
        ADSDKLog.Log("[GdtAds] In InitializeSdk: currentActivity: " + currentActivity);
    }

    public static String getGdtAppId() {
        if (sdkKey == null || sdkKey.length() == 0) {
            ADSDKLog.Log("[GdtAds] AppId is null");
            return "";
        }
        return sdkKey;
    }

    public static String CreateInstance(Activity activity, String type, String adUintId, IAdListener listener) {
        ADSDKLog.Log("[GdtAds] CreateInstance: type: " + type + ", adUintId: " + adUintId);
        if (objFactorys == null) {
            objFactorys = new AdObjectFactory();
        }

        return objFactorys.createInstance(activity, type, adUintId, listener);
    }

    public static Object Invoke(Context context, String className, String objUUId, String methodName) {
        if (objFactorys == null)
            return null;
        Object obj = objFactorys.getObject(objUUId);
        if (obj == null)
            return null;
        return BaseBridge.InvokeMethod(obj, context, className, methodName);
    }
}
