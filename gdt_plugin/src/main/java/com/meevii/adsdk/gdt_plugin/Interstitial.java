package com.meevii.adsdk.gdt_plugin;

import android.app.Activity;
import android.content.Context;

import com.meevii.adsdk.base_plugin.adcommon.ADSDKLog;
import com.meevii.adsdk.base_plugin.adcommon.IAdListener;
import com.meevii.adsdk.base_plugin.adcommon.ThreadHandler;
import com.qq.e.ads.interstitial.AbstractInterstitialADListener;
import com.qq.e.ads.interstitial.InterstitialAD;
import com.qq.e.comm.util.AdError;

public class Interstitial {

    private InterstitialAD mInterstitialAd;
    private IAdListener adListener;
    private boolean isLoaded;
    private String mAdUnitId;

    public Interstitial(final Context context, final String adUnitId, IAdListener adListener) {
        this.mAdUnitId = adUnitId;
        this.isLoaded = false;

        if (context instanceof Activity) {
            ADSDKLog.Log("[GdtAds] context is Activity");
        } else {
            ADSDKLog.Log("[GdtAds] context is NOT Activity");
        }

        mInterstitialAd = new InterstitialAD((Activity) context, AndroidBridge.getGdtAppId(), mAdUnitId);
        this.adListener = adListener;
    }

    public void loadAd(final Context context) {
        ADSDKLog.Log("[GdtAds] Interstitial loadAd");

        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                ADSDKLog.Log("[GdtAds] Interstitial loadAd in Handler");
                mInterstitialAd.setADListener(new AbstractInterstitialADListener() {
                    @Override
                    public void onADReceive() {
                        OnAdLoad();
                    }

                    @Override
                    public void onNoAD(AdError adError) {
                        OnAdFailedToLoad(adError.getErrorMsg(), adError.getErrorCode());
                    }

                    @Override
                    public void onADOpened() {

                    }

                    @Override
                    public void onADExposure() {

                    }

                    @Override
                    public void onADClicked() {
                        OnAdClicked();
                    }

                    @Override
                    public void onADClosed() {
                        OnAdClosed();
                    }

                    @Override
                    public void onADLeftApplication() {
                        OnAdLeftApplication();
                    }
                });
                mInterstitialAd.loadAD();
            }
        });
    }

    private void OnAdLoad() {
        ADSDKLog.Log("[GdtAds] Interstitial OnAdLoad");
        isLoaded = true;
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdLoaded();
                    }
                }
            }).start();
        }
    }

    private void OnAdFailedToLoad(final String errorMessage, final int errorCode) {
        ADSDKLog.Log("[GdtAds] Interstitial OnAdFailedToLoad: errorMessage: " + errorMessage + ", errorCode: " + errorCode);
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdFailedToLoad(errorMessage, errorCode);
                    }
                }
            }).start();
        }
    }

    private void OnAdClicked() {
        ADSDKLog.Log("[GdtAds] Interstitial OnAdClicked!");
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdClick();
                    }
                }
            }).start();
        }
    }

    private void OnAdClosed() {
        ADSDKLog.Log("[GdtAds] Interstitial OnAdClosed!");
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdClosed();
                    }
                }
            }).start();
        }
    }

    private void OnAdLeftApplication() {
        ADSDKLog.Log("[GdtAds] Interstitial OnAdLeftApplication");
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdLeftApplication();
                    }
                }
            }).start();
        }
    }

    public boolean isLoaded(Context context) {
        return isLoaded;
    }

    public void show(final Context context) {
        ADSDKLog.Log("[GdtAds] Interstitial OnAdLeftApplication");
        if (!isLoaded(null))
            return;
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                if (isLoaded(null)) {
                    if (mInterstitialAd != null) {
                        mInterstitialAd.show();
                    }
                }
            }
        });
    }

    public void destroy(Context context) {
        this.adListener = null;
        isLoaded = false;
    }

    public boolean isValid(Context context) {
        if (mInterstitialAd == null) {
            ADSDKLog.Log("[GdtAds] Interstitial mInterstitialAd is null, isValid: false");
            return false;
        }
        ADSDKLog.Log("[GdtAds] Interstitial mInterstitialAd is null, isValid: " + isLoaded);
        return isLoaded;
    }

}
