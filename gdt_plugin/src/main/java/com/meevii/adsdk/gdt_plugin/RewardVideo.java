package com.meevii.adsdk.gdt_plugin;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.PermissionChecker;

import com.meevii.adsdk.base_plugin.adcommon.ADSDKLog;
import com.meevii.adsdk.base_plugin.adcommon.IAdListener;
import com.meevii.adsdk.base_plugin.adcommon.IRewardAdListener;
import com.meevii.adsdk.base_plugin.adcommon.ThreadHandler;
import com.qq.e.ads.rewardvideo.RewardVideoAD;
import com.qq.e.ads.rewardvideo.RewardVideoADListener;
import com.qq.e.comm.util.AdError;

public class RewardVideo {

    private RewardVideoAD mInterstitialAd;
    private IAdListener adListener;
    private boolean isLoaded;
    private String mAdUnitId;

    private boolean isGotReward = false;

    public RewardVideo(final Context context, final String adUnitId, IAdListener adListener) {
        this.mAdUnitId = adUnitId;
        this.isLoaded = false;

        String[] var1 = new String[]{};
        for (int i = 0; i < var1.length; i++) {
            if (context.checkCallingOrSelfPermission(var1[i]) == PackageManager.PERMISSION_GRANTED || PermissionChecker.checkSelfPermission(context, var1[i]) == PackageManager.PERMISSION_GRANTED) {
                ADSDKLog.Log("[admob] Permission: " + var1[i] + " Success");
            } else {
                ActivityCompat.requestPermissions((Activity) context,
                        new String[]{"android.permission.WRITE_EXTERNAL_STORAGE"}, 1);
                ADSDKLog.Log("[admob] Permission: " + var1[i] + " Not Success");
            }
        }

        mInterstitialAd = new RewardVideoAD(context, AndroidBridge.getGdtAppId(), mAdUnitId, new RewardVideoADListener() {
            @Override
            public void onADLoad() {
                OnAdLoad();
            }

            @Override
            public void onVideoCached() {
            }

            @Override
            public void onADShow() {
            }

            @Override
            public void onADExpose() {
            }

            @Override
            public void onReward() {
                isGotReward = true;
            }

            @Override
            public void onADClick() {
                OnAdClicked();
            }

            @Override
            public void onVideoComplete() {
            }

            @Override
            public void onADClose() {
                OnAdClosed();
            }

            @Override
            public void onError(AdError adError) {
                OnAdFailedToLoad(adError.getErrorMsg(), adError.getErrorCode());
            }
        });
        this.adListener = adListener;
    }

    public void loadAd(final Context context) {
        ADSDKLog.Log("[GdtAds] RewardVideo loadAd");
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                ADSDKLog.Log("[GdtAds] RewardVideo loadAd in Handler");
                mInterstitialAd.loadAD();
            }
        });
    }

    private void OnAdLoad() {
        ADSDKLog.Log("[GdtAds] RewardVideo OnAdLoad");
        isLoaded = true;
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdLoaded();
                    }
                }
            }).start();
        }
    }

    private void OnAdFailedToLoad(final String errorMessage, final int errorCode) {
        ADSDKLog.Log("[GdtAds] RewardVideo OnAdFailedToLoad: errorMessage: " + errorMessage + ", errorCode: " + errorCode);
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdFailedToLoad(errorMessage, errorCode);
                    }
                }
            }).start();
        }
    }

    private void OnAdClicked() {
        ADSDKLog.Log("[admob] OnAdClicked!");
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdClick();
                    }
                }
            }).start();
        }
    }

    private void OnAdClosed() {
        ADSDKLog.Log("[admob] OnAdClosed!");
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        if (adListener instanceof IRewardAdListener) {
                            if (isGotReward) {
                                ((IRewardAdListener) adListener).onAdReward();
                            } else {
                                ((IRewardAdListener) adListener).onAdNotReward();
                            }
                        }
                        isGotReward = false;
                        adListener.onAdClosed();
                    }
                }
            }).start();
        }
    }

    public boolean isLoaded(Context context) {
        return isLoaded;
    }

    public void show(final Context context) {
        ADSDKLog.Log("[GdtAds] RewardVideo show");
        if (!isLoaded(null))
            return;
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                if (isLoaded(null)) {
                    mInterstitialAd.showAD();
                }
            }
        });
    }

    public void destroy(Context context) {
        this.adListener = null;
        isLoaded = false;
    }

    public boolean isValid(Context context) {
        if (mInterstitialAd == null) {
            ADSDKLog.Log("[GdtAds] RewardVideo mInterstitialAd is null, isValid: false");
            return false;
        }
        ADSDKLog.Log("[GdtAds] RewardVideo mInterstitialAd is null, isValid: " + isLoaded);
        return isLoaded;
    }
}
