package com.meevii.adsdk.gdt_plugin;

import android.app.Activity;
import android.content.Context;
import android.view.View;

import com.meevii.adsdk.base_plugin.adcommon.ADSDKLog;
import com.meevii.adsdk.base_plugin.adcommon.AdBanner;
import com.meevii.adsdk.base_plugin.adcommon.AdBannerSize;
import com.meevii.adsdk.base_plugin.adcommon.ThreadHandler;
import com.qq.e.ads.banner.ADSize;
import com.qq.e.ads.banner.BannerADListener;
import com.qq.e.ads.banner.BannerView;
import com.qq.e.comm.util.AdError;

public class Banner extends AdBanner {

    BannerView banner;

    private Banner(Context context, AdBannerSize adSize) {
        super(context, adSize);
    }

    public Banner(Context applicationContext, String adUnitId, AdBannerSize adSize) {
        super(applicationContext, adSize);
        create(adUnitId);
    }

    @Override
    protected View CreateInstance(Context context, String adId, AdBannerSize adSize) {
        banner = new BannerView((Activity) context, ADSize.BANNER, AndroidBridge.getGdtAppId(), adId);
        banner.setRefresh(0);
        mAdView = banner;
        return banner;
    }

    @Override
    protected void setListener(View adView) {
        ADSDKLog.Log("[GdtAds] Banner SetListener is Activity");
        banner.setADListener(new BannerADListener() {
            @Override
            public void onNoAD(AdError adError) {
                ADSDKLog.Log("[GdtAds] Banner No Ads, Code:" + adError.getErrorCode() + ", Message:" + adError.getErrorMsg());
                onAdFailedToLoad(adError.getErrorMsg(), adError.getErrorCode());
            }

            @Override
            public void onADReceiv() {
                ADSDKLog.Log("[GdtAds] Banner Load Success");
                onAdLoaded();
            }

            @Override
            public void onADExposure() {

            }

            @Override
            public void onADClosed() {
                onAdClosed();
            }

            @Override
            public void onADClicked() {
                onAdClick();
            }

            @Override
            public void onADLeftApplication() {
                onAdLeftApplication();
            }

            @Override
            public void onADOpenOverlay() {

            }

            @Override
            public void onADCloseOverlay() {

            }
        });
    }

    @Override
    protected void doLoadAd(View adView) {
        ADSDKLog.Log("[GdtAds] Banner Load Ads");
        if (banner != null) {
            banner.loadAD();
        }
    }

    @Override
    protected void doDestroy(View adView) {
        ADSDKLog.Log("[GdtAds] Banner doDestroy");
        if (banner != null && banner.isShown()) {
            ADSDKLog.Log("[GdtAds] Banner destroy");
            banner.destroy();
        }
    }

    public void destroyV2(Context context) {
        ADSDKLog.Log("[GdtAds] Banner destroyV2");
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                if (banner != null && banner.isShown()) {
                    ADSDKLog.Log("[GdtAds] Banner destroyV2");
                    banner.destroy();
                }
            }
        });

    }

    @Override
    protected void resumeAdView(View adView) {
        ADSDKLog.Log("[GdtAds] Banner resumeAdView");
        if (banner != null) {
            ADSDKLog.Log("[GdtAds] Banner VISIBLE");
            banner.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void pauseAdView(View adView) {
        ADSDKLog.Log("[GdtAds] Banner pauseAdView");
        if (banner != null && banner.isShown()) {
            ADSDKLog.Log("[GdtAds] Banner GONE");
            banner.setVisibility(View.GONE);
        }
    }
}
