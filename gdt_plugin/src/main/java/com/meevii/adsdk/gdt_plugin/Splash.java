package com.meevii.adsdk.gdt_plugin;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.meevii.adsdk.base_plugin.adcommon.ADSDKLog;
import com.meevii.adsdk.base_plugin.adcommon.IAdListener;
import com.meevii.adsdk.base_plugin.adcommon.ThreadHandler;
import com.qq.e.ads.splash.SplashAD;
import com.qq.e.ads.splash.SplashADListener;
import com.qq.e.comm.util.AdError;

public class Splash {

    private SplashAD splashAD;
    private IAdListener adListener;
    private boolean mHasLoaded;
    private String mAdUnitId;
    private FrameLayout mSplashContainer;
    private TextView mSplashSkip;
    private int AD_TIME_OUT = 10000;

    public Splash(final Context context, final String adUnitId, IAdListener adListener) {
        this.mAdUnitId = adUnitId;
        this.mHasLoaded = false;
        this.adListener = adListener;
    }

    public void loadAd(final Context context) {
        ADSDKLog.Log("[GDTAD] Splash loadAd");
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {

            public void onHandler() {
                mSplashContainer = ((Activity) context).findViewById(R.id.splash);
                mSplashSkip = ((Activity) context).findViewById(R.id.skip);
                mSplashContainer.setVisibility(View.VISIBLE);
                mSplashSkip.setVisibility(View.VISIBLE);
                splashAD = new SplashAD((Activity) context, mSplashContainer, mSplashSkip, AndroidBridge.getGdtAppId(), mAdUnitId, new SplashADListener() {
                    @Override
                    public void onADDismissed() {
                        goToMainActivity();
                        OnAdClosed();
                    }

                    @Override
                    public void onNoAD(AdError adError) {
                        OnAdFailedToLoad(adError.getErrorMsg(), adError.getErrorCode());
                    }

                    @Override
                    public void onADPresent() {
                        mSplashSkip.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onADClicked() {
                        OnAdClicked();
                    }

                    @Override
                    public void onADTick(long l) {
                        OnAdLoad();
                    }

                    @Override
                    public void onADExposure() {
                    }
                }, AD_TIME_OUT);
            }
        });
    }

    private void goToMainActivity() {
        if (mSplashContainer != null) {
            mSplashContainer.removeAllViews();
            mSplashContainer.setVisibility(View.GONE);
        }
        if (mSplashSkip != null) {
            mSplashSkip.setVisibility(View.GONE);
        }
    }

    private void OnAdLoad() {
        ADSDKLog.Log("[GDTAD] Splash OnAdLoad");
        mHasLoaded = true;
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdLoaded();
                    }
                }
            }).start();
        }
    }

    private void OnAdFailedToLoad(final String errorMessage, final int errorCode) {
        ADSDKLog.Log("[GDTAD] Splash OnAdFailedToLoad: errorMessage: " + errorMessage + ", errorCode: " + errorCode);
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdFailedToLoad(errorMessage, errorCode);
                    }
                }
            }).start();
        }
    }

    private void OnAdClicked() {
        ADSDKLog.Log("[GDTAD] Splash OnAdClicked!");
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdClick();
                    }
                }
            }).start();
        }
    }

    private void OnAdClosed() {
        ADSDKLog.Log("[GDTAD] Splash OnAdClosed!");
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdClosed();
                    }
                }
            }).start();
        }
    }

    public boolean isLoaded(Context context) {
        return mHasLoaded;
    }

    public void show(final Context context) {
        ADSDKLog.Log("[GDTAD] Splash OnAdLeftApplication");
        if (!isLoaded(null))
            return;
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                if (isLoaded(null)) {
                }
            }
        });
    }

    public void destroy(Context context) {
        this.adListener = null;
        mHasLoaded = false;
    }

    public boolean isValid(Context context) {
        ADSDKLog.Log("[GDTAD] Splash mSplashAd is null, isValid: " + mHasLoaded);
        return mHasLoaded;
    }
}
