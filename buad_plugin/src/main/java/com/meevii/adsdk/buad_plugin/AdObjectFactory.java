package com.meevii.adsdk.buad_plugin;

import android.app.Activity;

import com.meevii.adsdk.base_plugin.adcommon.ADSDKLog;
import com.meevii.adsdk.base_plugin.adcommon.IAdListener;

import java.util.HashMap;
import java.util.UUID;

public class AdObjectFactory {

    public HashMap<String, Object> objs = new HashMap<>();

    Object getObject(String id) {
        return objs.get(id);
    }


    public String createInstance(Activity activity, String type, String adUintId, IAdListener listener) {
        ADSDKLog.Log("[TTAds] CreateInstance type: " + type);
        Object obj = null;
        if (type.equals(Banner.class.getName())) {
            obj = new Banner(activity, adUintId);
            ((Banner) obj).setAdListener(listener);
            ADSDKLog.Log("[TTAds] CreateInstance Banner");
        } else if (type.equals(Interstitial.class.getName())) {
            obj = new Interstitial(activity, adUintId, listener);
            ADSDKLog.Log("[TTAds] CreateInstance Interstitial");
        } else if (type.equals(RewardVideo.class.getName())) {
            obj = new RewardVideo(activity, adUintId, listener);
            ADSDKLog.Log("[TTAds] CreateInstance RewardVideo");
        } else if (type.equals(Splash.class.getName())) {
            obj = new Splash(activity, adUintId, listener);
            ADSDKLog.Log("[TTAds] CreateInstance Splash");
        }
        if (obj != null) {
            String uuid = UUID.randomUUID().toString().replaceAll("-", "");
            objs.put(uuid, obj);
            return uuid;
        }
        return "";
    }

}
