package com.meevii.adsdk.buad_plugin;

import android.app.Activity;
import android.content.Context;

import com.bytedance.sdk.openadsdk.AdSlot;
import com.bytedance.sdk.openadsdk.TTAdConstant;
import com.bytedance.sdk.openadsdk.TTAdNative;
import com.bytedance.sdk.openadsdk.TTRewardVideoAd;
import com.meevii.adsdk.base_plugin.adcommon.ADSDKLog;
import com.meevii.adsdk.base_plugin.adcommon.IAdListener;
import com.meevii.adsdk.base_plugin.adcommon.IRewardAdListener;
import com.meevii.adsdk.base_plugin.adcommon.ThreadHandler;

public class RewardVideo {

    private TTRewardVideoAd mInterstitialAd;
    private IAdListener adListener;
    private boolean isLoaded;
    private String mAdUnitId;
    private AdSlot adSlot;

    private boolean isGotReward = false;

    public RewardVideo(final Context context, final String adUnitId, IAdListener adListener) {
        this.mAdUnitId = adUnitId;
        this.isLoaded = false;

        adSlot = new AdSlot.Builder()
                .setCodeId(mAdUnitId)
                .setSupportDeepLink(true)
                .setImageAcceptedSize(1080, 1920)
                .setOrientation(TTAdConstant.VERTICAL)
                .build();
        this.adListener = adListener;
    }

    public void loadAd(final Context context) {
        ADSDKLog.Log("[TTAds] RewardVideo loadAd");
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                ADSDKLog.Log("[TTAds] RewardVideo loadAd in Handler");
                AndroidBridge.getTTSdk().createAdNative(context).loadRewardVideoAd(adSlot, new TTAdNative.RewardVideoAdListener() {
                    @Override
                    public void onError(int errorCode, String message) {
                        OnAdFailedToLoad(ErrorCode.getErrorReason(errorCode), errorCode);
                    }

                    @Override
                    public void onRewardVideoAdLoad(TTRewardVideoAd ttRewardVideoAd) {
                        OnAdLoad();
                        mInterstitialAd = ttRewardVideoAd;
                        ttRewardVideoAd.setRewardAdInteractionListener(new TTRewardVideoAd.RewardAdInteractionListener() {
                            @Override
                            public void onAdShow() {
                            }

                            @Override
                            public void onAdVideoBarClick() {
                                OnAdClicked();
                                OnAdLeftApplication();
                            }

                            @Override
                            public void onAdClose() {
                                OnAdClosed();
                            }

                            @Override
                            public void onVideoComplete() {
                            }

                            @Override
                            public void onVideoError() {
                            }

                            @Override
                            public void onRewardVerify(boolean b, int i, String s) {
                                isGotReward = true;
                            }

                            @Override
                            public void onSkippedVideo() {
                            }
                        });
                    }

                    @Override
                    public void onRewardVideoCached() {
                    }
                });
            }
        });
    }

    private void OnAdLoad() {
        ADSDKLog.Log("[TTAds] RewardVideo OnAdLoad");
        isLoaded = true;
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdLoaded();
                    }
                }
            }).start();
        }
    }

    private void OnAdFailedToLoad(final String errorMessage, final int errorCode) {
        ADSDKLog.Log("[TTAds] RewardVideo OnAdFailedToLoad: errorMessage: " + errorMessage + ", errorCode: " + errorCode);
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdFailedToLoad(errorMessage, errorCode);
                    }
                }
            }).start();
        }
    }

    private void OnAdClicked() {
        ADSDKLog.Log("[admob] OnAdClicked!");
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdClick();
                    }
                }
            }).start();
        }
    }

    private void OnAdClosed() {
        ADSDKLog.Log("[admob] OnAdClosed!");
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        if (adListener instanceof IRewardAdListener) {
                            if (isGotReward) {
                                ((IRewardAdListener) adListener).onAdReward();
                            } else {
                                ((IRewardAdListener) adListener).onAdNotReward();
                            }
                        }
                        isGotReward = false;
                        adListener.onAdClosed();
                    }
                }
            }).start();
        }
    }

    private void OnAdLeftApplication() {
        ADSDKLog.Log("[TTAds] RewardVideo OnAdLeftApplication");
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdLeftApplication();
                    }
                }
            }).start();
        }
    }

    private void OnReward() {
        ADSDKLog.Log("[TTAds] RewardVideo OnReward");
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener instanceof IRewardAdListener) {
                        ((IRewardAdListener) adListener).onAdReward();
                    }
                }
            }).start();
        }
    }

    /**
     * Returns {@code True} if the interstitial has loaded.
     */
    public boolean isLoaded(Context context) {
        return isLoaded;
    }

    /**
     * Shows the interstitial if it has loaded.
     */
    public void show(final Context context) {
        ADSDKLog.Log("[TTAds] RewardVideo Show");
        if (!isLoaded(null))
            return;
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                if (isLoaded(null)) {
                    mInterstitialAd.showRewardVideoAd((Activity) context);
                }
            }
        });
    }

    public void destroy(Context context) {
        this.adListener = null;
        isLoaded = false;
    }

    public boolean isValid(Context context) {
        if (mInterstitialAd == null) {
            ADSDKLog.Log("[TTAds] RewardVideo mInterstitialAd is null, isValid: false");
            return false;
        }
        ADSDKLog.Log("[TTAds] RewardVideo mInterstitialAd is null, isValid: " + isLoaded);
        return isLoaded;
    }
}
