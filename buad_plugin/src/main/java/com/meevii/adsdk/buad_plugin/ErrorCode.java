package com.meevii.adsdk.buad_plugin;

public class ErrorCode {
    public static String getErrorReason(int errorCode) {
        if (errorCode == 20001)
            return "no fill";
        if (errorCode == -2)
            return "net work error";
        return "" + errorCode;
    }
}
