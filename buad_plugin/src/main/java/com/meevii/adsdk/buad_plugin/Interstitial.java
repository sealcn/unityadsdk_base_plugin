package com.meevii.adsdk.buad_plugin;

import android.app.Activity;
import android.content.Context;

import com.bytedance.sdk.openadsdk.AdSlot;
import com.bytedance.sdk.openadsdk.TTAdConstant;
import com.bytedance.sdk.openadsdk.TTAdNative;
import com.bytedance.sdk.openadsdk.TTFullScreenVideoAd;
import com.meevii.adsdk.base_plugin.adcommon.ADSDKLog;
import com.meevii.adsdk.base_plugin.adcommon.IAdListener;
import com.meevii.adsdk.base_plugin.adcommon.ThreadHandler;

public class Interstitial {

    private TTFullScreenVideoAd mFullScreenVideoAd;
    private IAdListener adListener;
    private boolean isLoaded;
    private String mAdUnitId;
    private AdSlot adSlot;

    public Interstitial(final Context context, final String adUnitId, IAdListener adListener) {
        this.mAdUnitId = adUnitId;
        this.isLoaded = false;

        adSlot = new AdSlot.Builder()
                .setCodeId(mAdUnitId)
                .setSupportDeepLink(true)
                .setImageAcceptedSize(1080, 1920)
                .setOrientation(TTAdConstant.VERTICAL)//必填参数，期望视频的播放方向：TTAdConstant.HORIZONTAL 或 TTAdConstant.VERTICAL
                .build();
        this.adListener = adListener;
    }

    public void loadAd(final Context context) {
        ADSDKLog.Log("[TTAds] FullScreenVideo loadAd");

        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                ADSDKLog.Log("[TTAds] FullScreenVideo loadAd in Handler");
                AndroidBridge.getTTSdk().createAdNative(context).loadFullScreenVideoAd(adSlot, new TTAdNative.FullScreenVideoAdListener() {
                    @Override
                    public void onError(int errorCode, String errorMessage) {
                        OnAdFailedToLoad(ErrorCode.getErrorReason(errorCode), errorCode);
                    }

                    @Override
                    public void onFullScreenVideoAdLoad(TTFullScreenVideoAd ttFullScreenVideoAd) {
                        OnAdLoad();
                        mFullScreenVideoAd = ttFullScreenVideoAd;
                        mFullScreenVideoAd.setFullScreenVideoAdInteractionListener(new TTFullScreenVideoAd.FullScreenVideoAdInteractionListener() {

                            @Override
                            public void onAdShow() {
                            }

                            @Override
                            public void onAdVideoBarClick() {
                                OnAdClicked();
                                OnAdLeftApplication();
                            }

                            @Override
                            public void onAdClose() {
                                OnAdClosed();
                            }

                            @Override
                            public void onVideoComplete() {

                            }

                            @Override
                            public void onSkippedVideo() {

                            }
                        });
                    }

                    @Override
                    public void onFullScreenVideoCached() {
                    }
                });
            }
        });
    }

    private void OnAdLoad() {
        ADSDKLog.Log("[TTAds] FullScreenVideo OnAdLoad");
        isLoaded = true;
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdLoaded();
                    }
                }
            }).start();
        }
    }

    private void OnAdFailedToLoad(final String errorMessage, final int errorCode) {
        ADSDKLog.Log("[TTAds] FullScreenVideo OnAdFailedToLoad: errorMessage: " + errorMessage + ", errorCode: " + errorCode);
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdFailedToLoad(errorMessage, errorCode);
                    }
                }
            }).start();
        }
    }

    private void OnAdClicked() {
        ADSDKLog.Log("[TTAds] FullScreenVideo OnAdClicked!");
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdClick();
                    }
                }
            }).start();
        }
    }

    private void OnAdClosed() {
        ADSDKLog.Log("[TTAds] FullScreenVideo OnAdClosed!");
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdClosed();
                    }
                }
            }).start();
        }
    }

    private void OnAdLeftApplication() {
        ADSDKLog.Log("[TTAds] FullScreenVideo OnAdLeftApplication");
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdLeftApplication();
                    }
                }
            }).start();
        }
    }

    public boolean isLoaded(Context context) {
        return isLoaded;
    }

    public void show(final Context context) {
        ADSDKLog.Log("[TTAds] FullScreenVideo OnAdLeftApplication");
        if (!isLoaded(null))
            return;
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                if (isLoaded(null)) {
                    if (mFullScreenVideoAd != null) {
                        mFullScreenVideoAd.showFullScreenVideoAd((Activity) context);
                    }
                }
            }
        });
    }

    public void destroy(Context context) {
        this.adListener = null;
        isLoaded = false;
    }

    public boolean isValid(Context context) {
        if (mFullScreenVideoAd == null) {
            ADSDKLog.Log("[TTAds] FullScreenVideo mFullScreenVideoAd is null, isValid: false");
            return false;
        }
        ADSDKLog.Log("[TTAds] FullScreenVideo mFullScreenVideoAd is null, isValid: " + isLoaded);
        return isLoaded;
    }

}
