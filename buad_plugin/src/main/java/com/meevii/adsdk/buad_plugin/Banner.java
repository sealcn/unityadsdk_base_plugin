package com.meevii.adsdk.buad_plugin;

import android.app.Activity;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;

import com.bytedance.sdk.openadsdk.AdSlot;
import com.bytedance.sdk.openadsdk.TTAdDislike;
import com.bytedance.sdk.openadsdk.TTAdNative;
import com.bytedance.sdk.openadsdk.TTNativeExpressAd;
import com.meevii.adsdk.base_plugin.adcommon.ADSDKLog;
import com.meevii.adsdk.base_plugin.adcommon.AdBanner;
import com.meevii.adsdk.base_plugin.adcommon.AdBannerSize;

import java.util.List;

public class Banner extends AdBanner {

    private View mAdView;
    private TTNativeExpressAd mBannerAd;
    private AdSlot adSlot;
    private float expressViewWidth = 350;
    private Context mContext;

    private Banner(Context context, AdBannerSize adSize) {
        super(context, adSize);
    }

    @Override
    protected View CreateInstance(Context context, String adId, AdBannerSize adSize) {
        mContext = context;
        return mAdView;
    }

    @Override
    protected void setListener(View adView) {
    }

    @Override
    protected void doLoadAd(View adView) {
    }

    public Banner(Context applicationContext, String adUnitId) {
        super(applicationContext, AdBannerSize.BANNER);
        create(adUnitId);

        adSlot = new AdSlot.Builder()
                .setCodeId(adUnitId)
                .setSupportDeepLink(true)
                .setAdCount(1)
                .setExpressViewAcceptedSize(expressViewWidth, 0)
                .setImageAcceptedSize(640, 320)
                .build();
    }

    @Override
    public void loadAd(Context context) {
        ADSDKLog.Log("[TTAds] Banner loadAd");
        AndroidBridge.getTTSdk().createAdNative(context).loadBannerExpressAd(adSlot, new TTAdNative.NativeExpressAdListener() {
            @Override
            public void onError(int code, String message) {
                ADSDKLog.Log("[TTAds] Banner onError:code:" + code + ", Message:" + message);
                onAdFailedToLoad(message, code);
            }

            @Override
            public void onNativeExpressAdLoad(List<TTNativeExpressAd> list) {
                ADSDKLog.Log("[TTAds] Banner onBannerAdLoad");
                if (list == null || list.size() == 0) {
                    onAdFailedToLoad("Size is Zero", 0);
                    return;
                }
                onAdLoaded();
                mBannerAd = list.get(0);
                mBannerAd.setExpressInteractionListener(new TTNativeExpressAd.ExpressAdInteractionListener() {
                    @Override
                    public void onAdClicked(View view, int i) {
                        onAdClick();
                        onAdLeftApplication();
                    }

                    @Override
                    public void onAdShow(View view, int i) {
                    }

                    @Override
                    public void onRenderFail(View view, String s, int i) {
                    }

                    @Override
                    public void onRenderSuccess(View view, float v, float v1) {
                    }
                });
                bindDislike(mBannerAd);
                mAdView = mBannerAd.getExpressAdView();
                Banner.super.mAdView = mAdView;
                mBannerAd.render();
            }
        });
    }


    private void bindDislike(TTNativeExpressAd ad) {
        ad.setDislikeCallback((Activity) mContext, new TTAdDislike.DislikeInteractionCallback() {
            @Override
            public void onSelected(int position, String value) {
                hide(mContext);
            }

            @Override
            public void onCancel() {
            }
        });
    }

    @Override
    protected boolean isCustomShow() {
        return true;
    }

    @Override
    protected void customShow(Activity activity) {

        if (mAdView.getParent() != null) {
            return;
        }

        mAdView.setVisibility(View.VISIBLE);
        ViewGroup root = (ViewGroup) activity.findViewById(android.R.id.content);

        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.BOTTOM | Gravity.CENTER;
        root.addView(mAdView, params);

    }

    @Override
    protected void resumeAdView(View adView) {
    }

    @Override
    protected void pauseAdView(View adView) {
    }

    @Override
    public boolean isUpdateWidth() {
        return true;
    }

    @Override
    protected void doDestroy(View adView) {
        if (mAdView != null) {
            ViewParent parentView = mAdView.getParent();
            if (parentView instanceof ViewGroup) {
                ((ViewGroup) parentView).removeView(mAdView);
            }

            mAdView.setVisibility(View.GONE);
        }
        mAdView = null;
    }
}
