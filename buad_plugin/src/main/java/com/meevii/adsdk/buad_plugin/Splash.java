package com.meevii.adsdk.buad_plugin;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;

import com.bytedance.sdk.openadsdk.AdSlot;
import com.bytedance.sdk.openadsdk.TTAdNative;
import com.bytedance.sdk.openadsdk.TTSplashAd;
import com.meevii.adsdk.base_plugin.adcommon.ADSDKLog;
import com.meevii.adsdk.base_plugin.adcommon.IAdListener;
import com.meevii.adsdk.base_plugin.adcommon.ThreadHandler;

public class Splash {

    private IAdListener adListener;
    private boolean mHasLoaded;
    private String mAdUnitId;
    private AdSlot adSlot;
    private FrameLayout mSplashContainer;
    private int AD_TIME_OUT = 10000;

    public Splash(final Context context, final String adUnitId, IAdListener adListener) {
        this.mAdUnitId = adUnitId;
        this.mHasLoaded = false;
        this.adListener = adListener;
        adSlot = new AdSlot.Builder()
                .setCodeId(mAdUnitId)
                .setSupportDeepLink(true)
                .setImageAcceptedSize(1080, 1920)
                .build();
    }

    public void loadAd(final Context context) {
        ADSDKLog.Log("[TTAds] Splash loadAd");
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {

            public void onHandler() {
                mSplashContainer = ((Activity) context).findViewById(R.id.splash);
                mSplashContainer.setVisibility(View.VISIBLE);
                AndroidBridge.getTTSdk().createAdNative(context).loadSplashAd(adSlot, new TTAdNative.SplashAdListener() {

                    public void onError(int code, String message) {
                        ADSDKLog.Log("[TTAds] Splash onError message:" + message);
                        mHasLoaded = true;
                        goToMainActivity();
                        OnAdFailedToLoad(message, code);
                    }


                    public void onTimeout() {
                        ADSDKLog.Log("[TTAds] Splash onTimeout");
                        mHasLoaded = true;
                        goToMainActivity();
                        OnAdFailedToLoad("Timeout", -100);
                    }


                    public void onSplashAdLoad(TTSplashAd ad) {
                        ADSDKLog.Log("[TTAds] Splash onSplashAdLoad");
                        OnAdLoad();
                        if (ad == null) {
                            return;
                        }
                        View view = ad.getSplashView();

                        mSplashContainer.setVisibility(View.VISIBLE);

                        mSplashContainer.removeAllViews();
                        mSplashContainer.addView(view);
                        ad.setSplashInteractionListener(new TTSplashAd.AdInteractionListener() {

                            public void onAdClicked(View view, int type) {
                                OnAdClicked();
                            }


                            public void onAdShow(View view, int type) {
                            }


                            public void onAdSkip() {
                                goToMainActivity();
                                OnAdClosed();
                            }


                            public void onAdTimeOver() {
                                goToMainActivity();
                                OnAdClosed();
                            }
                        });
                    }
                }, AD_TIME_OUT);
            }
        });
    }

    private void goToMainActivity() {
        if (mSplashContainer != null) {
            mSplashContainer.removeAllViews();
            mSplashContainer.setVisibility(View.GONE);
        }
    }

    private void OnAdLoad() {
        ADSDKLog.Log("[TTAds] Splash OnAdLoad");
        mHasLoaded = true;
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdLoaded();
                    }
                }
            }).start();
        }
    }

    private void OnAdFailedToLoad(final String errorMessage, final int errorCode) {
        ADSDKLog.Log("[TTAds] Splash OnAdFailedToLoad: errorMessage: " + errorMessage + ", errorCode: " + errorCode);
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdFailedToLoad(errorMessage, errorCode);
                    }
                }
            }).start();
        }
    }

    private void OnAdClicked() {
        ADSDKLog.Log("[TTAds] Splash OnAdClicked!");
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdClick();
                    }
                }
            }).start();
        }
    }

    private void OnAdClosed() {
        ADSDKLog.Log("[TTAds] Splash OnAdClosed!");
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdClosed();
                    }
                }
            }).start();
        }
    }

    public boolean isLoaded(Context context) {
        return mHasLoaded;
    }

    public void show(final Context context) {
        ADSDKLog.Log("[TTAds] Splash OnAdLeftApplication");
        if (!isLoaded(null))
            return;
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                if (isLoaded(null)) {
                }
            }
        });
    }

    public void destroy(Context context) {
        this.adListener = null;
        mHasLoaded = false;
    }

    public boolean isValid(Context context) {
        ADSDKLog.Log("[TTAds] Splash mSplashAd is null, isValid: " + mHasLoaded);
        return mHasLoaded;
    }
}
