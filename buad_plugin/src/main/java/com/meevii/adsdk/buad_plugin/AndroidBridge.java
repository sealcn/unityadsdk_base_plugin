package com.meevii.adsdk.buad_plugin;

import android.app.Activity;
import android.content.Context;

import com.bytedance.sdk.openadsdk.TTAdManager;
import com.bytedance.sdk.openadsdk.TTAdManagerFactory;
import com.meevii.adsdk.base_plugin.adcommon.ADSDKLog;
import com.meevii.adsdk.base_plugin.adcommon.BaseBridge;
import com.meevii.adsdk.base_plugin.adcommon.IAdListener;

public class AndroidBridge extends BaseBridge {

    private static AdObjectFactory objFactorys;
    public static TTAdManager sdk;

    private static String sdkKey;
    private static String sdkName;

    public static TTAdManager getTTSdk() {
        return sdk;
    }

    //-------------------初始化相关------------------------------

    public static void SetSdkKey(String key) {
        ADSDKLog.Log("[TTAds] In SetSdkKey");
        sdkKey = key;
        ADSDKLog.Log("[TTAds] In SetSdkKey: key: " + key);
        if (sdk != null && sdkKey != null) {
            ADSDKLog.Log("[TTAds] In SetSdkKey: key: " + key);
            sdk.setAppId(sdkKey);
            ADSDKLog.Log("[TTAds] SetSdkKey finish");
        }
    }

    public static void SetAppName(String name) {
        ADSDKLog.Log("[TTAds] In SetAppName");
        sdkName = name;
        ADSDKLog.Log("[TTAds] In SetAppName: name: " + name);
        if (sdk != null && sdkName != null) {
            ADSDKLog.Log("[TTAds] In SetSdkKey: name: " + name);
            sdk.setName(sdkName);
            ADSDKLog.Log("[TTAds] SetAppName finish");
        }
    }

    public static void InitializeSdk(Activity currentActivity) {
        ADSDKLog.Log("[TTAds] In InitializeSdk");
        ADSDKLog.Log("[TTAds] In InitializeSdk: currentActivity: " + currentActivity);
        if (sdk == null) {
            ADSDKLog.Log("[TTAds] In InitializeSdk: sdk == null");
            sdk = TTAdManagerFactory.getInstance(currentActivity);
            if (sdkKey != null) {
                ADSDKLog.Log("[TTAds] In InitializeSdk: sdkKey != null");
                sdk.setAppId(sdkKey);
                ADSDKLog.Log("[TTAds] InitializeSdk finish key");
            }
            if (sdkName != null) {
                ADSDKLog.Log("[TTAds] In InitializeSdk: name != null");
                sdk.setName(sdkName);
                ADSDKLog.Log("[TTAds] InitializeSdk finish name");
            }
        }
    }

    //-------------------end of 初始化相关------------------------------

    //for unity
    public static String CreateInstance(Activity activity, String type, String adUintId, IAdListener listener) {
        ADSDKLog.Log("[TTAds] CreateInstance: type: " + type + ", adUintId: " + adUintId);
        if (objFactorys == null) {
            objFactorys = new AdObjectFactory();
        }

        return objFactorys.createInstance(activity, type, adUintId, listener);
    }

    //invoke
    public static Object Invoke(Context context, String className, String objUUId, String methodName) {
        if (objFactorys == null) {
            return null;
        }
        Object obj = objFactorys.getObject(objUUId);
        if (obj == null) {
            return null;
        }
        return BaseBridge.InvokeMethod(obj, context, className, methodName);
    }
}
