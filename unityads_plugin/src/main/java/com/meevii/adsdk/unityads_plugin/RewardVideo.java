package com.meevii.adsdk.unityads_plugin;

import android.app.Activity;

import com.meevii.adsdk.base_plugin.adcommon.IAdListener;
import com.meevii.adsdk.base_plugin.adcommon.IRewardAdListener;
import com.unity3d.ads.UnityAds;

public class RewardVideo extends Interstitial {

    private boolean isGotReward = false;

    public RewardVideo(final Activity activity, String adId, IAdListener adListener) {
        super(activity, adId, adListener);
    }

    @Override
    protected void OnADClosed(final String placementId, final UnityAds.FinishState finishState) {
        if (mAdListener != null && mAdListener instanceof IRewardAdListener) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (mADId != null && mADId.equals(placementId)) {
                        isGotReward = finishState == UnityAds.FinishState.COMPLETED;
                        if (mAdListener != null) {
                            if (mAdListener instanceof IRewardAdListener) {
                                if (isGotReward) {
                                    ((IRewardAdListener) mAdListener).onAdReward();
                                } else {
                                    ((IRewardAdListener) mAdListener).onAdNotReward();
                                }
                            }
                            isGotReward = false;
                            mAdListener.onAdClosed();
                        }
                    }
                }
            }).start();
        }
    }
}
