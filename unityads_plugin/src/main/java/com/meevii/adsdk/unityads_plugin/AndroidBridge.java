package com.meevii.adsdk.unityads_plugin;

import android.app.Activity;
import android.content.Context;

import com.meevii.adsdk.base_plugin.adcommon.BaseBridge;
import com.meevii.adsdk.base_plugin.adcommon.IAdListener;

public class AndroidBridge extends BaseBridge {

    private static AdObjectFactory objFactorys;

    public static void Initialize(Activity activity, String gameId, boolean bTestMode) {
        UnityUtils.Initialize(activity, gameId, bTestMode);
    }

    public static String CreateInstance(Activity activity, String type, String adUintId, IAdListener listener) {
        if (objFactorys == null) {
            objFactorys = new AdObjectFactory();
        }

        return objFactorys.createInstance(activity, type, adUintId, listener);
    }

    public static Object Invoke(Context context, String className, String objUUId, String methodName) {
        if (objFactorys == null)
            return null;
        Object obj = objFactorys.getObject(objUUId);
        if (obj == null)
            return null;
        return BaseBridge.InvokeMethod(obj, context, className, methodName);
    }
}
