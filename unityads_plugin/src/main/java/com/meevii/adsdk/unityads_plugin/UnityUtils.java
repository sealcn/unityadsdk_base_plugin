package com.meevii.adsdk.unityads_plugin;

import android.app.Activity;

import com.meevii.adsdk.base_plugin.adcommon.ADSDKLog;
import com.meevii.adsdk.base_plugin.adcommon.ThreadHandler;
import com.unity3d.ads.IUnityAdsListener;
import com.unity3d.ads.UnityAds;

import java.util.HashMap;

public class UnityUtils {
    public static void Initialize(Activity activity, String gameId, boolean bTestMode) {
        IUnityAdsListener listener = new IUnityAdsListener() {
            @Override
            public void onUnityAdsReady(String placementId) {
                ADSDKLog.Log("[unity] onUnityAdsReady for " + placementId);
                IUnityAdsListener listener = getAdListener(placementId);
                if (listener != null) {
                    listener.onUnityAdsReady(placementId);
                }
            }

            @Override
            public void onUnityAdsStart(String placementId) {
                ADSDKLog.Log("onUnityAdsStart for " + placementId);
                IUnityAdsListener listener = getAdListener(placementId);
                if (listener != null) {
                    listener.onUnityAdsStart(placementId);
                }
            }

            @Override
            public void onUnityAdsFinish(String placementId, UnityAds.FinishState finishState) {
                ADSDKLog.Log("[unity] onUnityAdsFinish for " + placementId);
                IUnityAdsListener listener = getAdListener(placementId);
                if (listener != null) {
                    listener.onUnityAdsFinish(placementId, finishState);
                }
            }

            @Override
            public void onUnityAdsError(UnityAds.UnityAdsError unityAdsError, String placementId) {
                ADSDKLog.Log("[unity] onUnityAdsError for " + placementId);
                IUnityAdsListener listener = getAdListener(placementId);
                if (listener != null) {
                    listener.onUnityAdsError(unityAdsError, placementId);
                }
            }
        };

        UnityAds.initialize(activity, gameId, listener, bTestMode);
    }

    private static HashMap<String, IUnityAdsListener> listeners = null;

    public static void setAdListener(final String placementId, IUnityAdsListener adsListener) {
        if (listeners == null) {
            listeners = new HashMap<>();
        }

        listeners.remove(placementId);
        if (adsListener != null) {
            listeners.put(placementId, adsListener);
        }
        final IUnityAdsListener listener = getAdListener(placementId);
        if (listener != null && UnityAds.isReady(placementId)) {
            ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
                @Override
                public void onHandler() {
                    listener.onUnityAdsReady(placementId);
                }
            });
        }
    }

    private static IUnityAdsListener getAdListener(String placementId) {
        if (listeners == null) {
            return null;
        }
        return listeners.get(placementId);
    }

}
