package com.meevii.adsdk.unityads_plugin;

import android.app.Activity;
import android.content.Context;

import com.meevii.adsdk.base_plugin.adcommon.IAdListener;
import com.meevii.adsdk.base_plugin.adcommon.ThreadHandler;
import com.unity3d.ads.IUnityAdsListener;
import com.unity3d.ads.UnityAds;

public class Interstitial {

    protected String mADId;
    protected IAdListener mAdListener;

    public Interstitial(final Activity activity, String adId, IAdListener adListener) {
        mADId = adId;
        mAdListener = adListener;
        registerEvent();
    }

    private void registerEvent() {
        UnityUtils.setAdListener(mADId, new IUnityAdsListener() {
            @Override
            public void onUnityAdsReady(String placementId) {
                OnADReady();
            }

            @Override
            public void onUnityAdsStart(String placementId) {
            }

            @Override
            public void onUnityAdsFinish(String placementId, UnityAds.FinishState finishState) {
                OnADClosed(placementId, finishState);
            }

            @Override
            public void onUnityAdsError(UnityAds.UnityAdsError unityAdsError, String strError) {
            }
        });
    }

    public void show(final Context context) {
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                if (isReady(null)) {
                    UnityAds.show((Activity) context, mADId);
                }
            }
        });
    }

    public boolean isReady(Context context) {
        return UnityAds.isReady(mADId);
    }

    protected void OnADClosed(String placementId, UnityAds.FinishState finishState) {
        if (mAdListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (mAdListener != null) {
                        mAdListener.onAdClosed();
                    }
                }
            }).start();
        }
    }

    protected void OnADReady() {
        if (mAdListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (mAdListener != null) {
                        mAdListener.onAdLoaded();
                    }
                }
            }).start();
        }
    }
}
