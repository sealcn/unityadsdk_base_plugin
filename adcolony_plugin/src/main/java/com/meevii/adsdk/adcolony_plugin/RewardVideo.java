package com.meevii.adsdk.adcolony_plugin;

import android.app.Activity;
import android.content.Context;

import com.adcolony.sdk.AdColony;
import com.adcolony.sdk.AdColonyInterstitial;
import com.adcolony.sdk.AdColonyInterstitialListener;
import com.adcolony.sdk.AdColonyReward;
import com.adcolony.sdk.AdColonyRewardListener;
import com.adcolony.sdk.AdColonyZone;
import com.meevii.adsdk.base_plugin.adcommon.IAdListener;
import com.meevii.adsdk.base_plugin.adcommon.IRewardAdListener;
import com.meevii.adsdk.base_plugin.adcommon.ThreadHandler;

public class RewardVideo {

    private AdColonyInterstitial mRewardAd;
    private IAdListener adListener;
    private String mAdUnitId;

    private boolean isGotReward = false;

    public RewardVideo(final Context context, final String adUnitId) {
        mAdUnitId = adUnitId;

        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                AdObjectFactory.AddIds(adUnitId);
                AdColony.configure((Activity) context, AndroidBridge.mAppId, AdObjectFactory.GetIds());
            }
        });
    }

    public void setAdListener(IAdListener adListener) {
        this.adListener = adListener;
    }

    private void OnAdLoad() {
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdLoaded();
                    }
                }
            }).start();
        }
    }

    private void OnAdFailedToLoad(final String errorMessage, final int errorCode) {
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdFailedToLoad(errorMessage, errorCode);
                    }
                }
            }).start();
        }
    }

    private void OnAdClosed() {
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        if (adListener instanceof IRewardAdListener) {
                            if (isGotReward) {
                                ((IRewardAdListener) adListener).onAdReward();
                            } else {
                                ((IRewardAdListener) adListener).onAdNotReward();
                            }
                        }
                        isGotReward = false;
                        adListener.onAdClosed();
                    }
                }
            }).start();
        }
    }

    private void OnClick() {
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdClick();
                    }
                }
            }).start();
        }
    }

    private void OnLeftApplication() {
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdLeftApplication();
                    }
                }
            }).start();
        }
    }

    public void loadAd(Context context) {
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                AdColony.requestInterstitial(mAdUnitId, new AdColonyInterstitialListener() {
                    @Override
                    public void onRequestFilled(AdColonyInterstitial adColonyInterstitial) {
                        mRewardAd = adColonyInterstitial;
                        OnAdLoad();
                    }

                    public void onRequestNotFilled(AdColonyZone zone) {
                        OnAdFailedToLoad(AdColonyUtils.getErrorReason(-1), -1);
                    }

                    public void onClosed(AdColonyInterstitial ad) {
                        OnAdClosed();
                    }

                    public void onExpiring(AdColonyInterstitial ad) {
                        OnAdFailedToLoad(AdColonyUtils.getErrorReason(-1), -1);
                    }

                    public void onLeftApplication(AdColonyInterstitial ad) {
                        OnLeftApplication();
                    }

                    public void onClicked(AdColonyInterstitial ad) {
                        OnClick();
                    }

                });
                AdColony.setRewardListener(new AdColonyRewardListener() {
                    @Override
                    public void onReward(AdColonyReward adColonyReward) {
                        isGotReward = true;
                    }
                });
            }
        });
    }

    public void show(Context context) {
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                mRewardAd.show();
            }
        });
    }

    public void destroy(Context context) {
        mRewardAd = null;
        this.adListener = null;
    }
}