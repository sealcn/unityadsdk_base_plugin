package com.meevii.adsdk.adcolony_plugin;

import android.app.Activity;
import android.content.Context;

import com.adcolony.sdk.AdColony;
import com.adcolony.sdk.AdColonyInterstitial;
import com.adcolony.sdk.AdColonyInterstitialListener;
import com.adcolony.sdk.AdColonyZone;
import com.meevii.adsdk.base_plugin.adcommon.ADSDKLog;
import com.meevii.adsdk.base_plugin.adcommon.IAdListener;
import com.meevii.adsdk.base_plugin.adcommon.ThreadHandler;

public class Interstitial {

    private AdColonyInterstitial mAdColonyInterstitial;
    private IAdListener adListener;
    private String mAdUnitId;

    public Interstitial(final Context context, final String adUnitId) {
        this.mAdUnitId = adUnitId;
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                AdObjectFactory.AddIds(adUnitId);
                AdColony.configure((Activity) context, AndroidBridge.mAppId, AdObjectFactory.GetIds());
            }
        });
    }

    public void setAdListener(IAdListener adListener) {
        this.adListener = adListener;
    }

    public void loadAd(Context context) {

        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                AdColony.requestInterstitial(mAdUnitId, new AdColonyInterstitialListener() {
                    @Override
                    public void onRequestFilled(AdColonyInterstitial adColonyInterstitial) {
                        mAdColonyInterstitial = adColonyInterstitial;
                        OnAdLoad();
                    }

                    @Override
                    public void onRequestNotFilled(AdColonyZone zone) {
                        OnAdFailedToLoad(AdColonyUtils.getErrorReason(-1), -1);
                    }

                    public void onClosed(AdColonyInterstitial ad) {
                        OnAdClosed();
                    }

                    public void onExpiring(AdColonyInterstitial ad) {
                        OnAdFailedToLoad("timeout", -1);
                    }

                    public void onLeftApplication(AdColonyInterstitial ad) {
                        OnAdLeftApplication();
                    }

                    public void onClicked(AdColonyInterstitial ad) {
                        OnAdClicked();
                    }
                });
            }
        });
    }

    private void OnAdLoad() {
        ADSDKLog.Log("[AdColony] OnAdLoad!");
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdLoaded();
                    }
                }
            }).start();
        }
    }

    private void OnAdFailedToLoad(final String errorMessage, final int errorCode) {
        ADSDKLog.Log("[AdColony] OnAdFailedToLoad!");
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdFailedToLoad(errorMessage, errorCode);
                    }
                }
            }).start();
        }
    }

    private void OnAdClicked() {
        ADSDKLog.Log("[AdColony] OnAdClicked!");
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdClick();
                    }
                }
            }).start();
        }
    }

    private void OnAdClosed() {
        ADSDKLog.Log("[AdColony] OnAdClosed!");
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdClosed();
                    }
                }
            }).start();
        }
    }

    private void OnAdLeftApplication() {
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdLeftApplication();
                    }
                }
            }).start();
        }
    }

    public void show(Context context) {
        ADSDKLog.Log("[AdColony] show");
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                if (mAdColonyInterstitial != null) {
                    mAdColonyInterstitial.show();
                }
            }
        });
    }

    public void destroy(Context context) {
        this.adListener = null;
        mAdColonyInterstitial = null;
    }
}
