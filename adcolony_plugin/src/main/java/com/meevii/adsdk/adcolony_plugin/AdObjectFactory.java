package com.meevii.adsdk.adcolony_plugin;

import android.app.Activity;

import com.meevii.adsdk.base_plugin.adcommon.IAdListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class AdObjectFactory {

    public HashMap<String, Object> objs = new HashMap<>();

    Object getObject(String id) {
        return objs.get(id);
    }

    public String createInstance(Activity activity, String type, String adUintId, IAdListener listener) {

        Object obj = null;
        if (type.equals(Interstitial.class.getName())) {
            Interstitial interstitial = new Interstitial(activity, adUintId);
            interstitial.setAdListener(listener);
            obj = interstitial;
        } else if (type.equals(RewardVideo.class.getName())) {
            RewardVideo rewardVideo = new RewardVideo(activity, adUintId);
            rewardVideo.setAdListener(listener);
            obj = rewardVideo;
        }
        if (obj != null) {
            String uuid = UUID.randomUUID().toString().replaceAll("-", "");
            objs.put(uuid, obj);
            return uuid;
        }
        return "";
    }

    public static ArrayList<String> ids;

    public static void AddIds(String id) {
        if (ids == null) {
            ids = new ArrayList<>();
        }
        if (!ids.contains(id)) {
            ids.add(id);
        }
    }

    public static String[] GetIds() {
        if (ids == null) {
            ids = new ArrayList<>();
        }
        String[] list = new String[ids.size()];
        return ids.toArray(list);
    }

}
