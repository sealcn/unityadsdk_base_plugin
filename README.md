# UnityADSDK_base_plugin
sdk 与 各家广告android平台的桥接, Plugin版本标识最早应用于UnitySDK-2.4中。

## 出包方式

在项目的根目录下执行`./gradlew build`, 输出所有plugin当前版本aar包到plugin下build/outputs/aar下。

## Release Note

### BasePlugin

#### V1.0.0

* 添加版本号记录

### AdColonyPlugin

#### V1.0.0

* 添加版本号记录

### AdMobPlugin

#### V1.0.0

* 添加版本号记录

### AppLovinPlugin

#### V1.0.0

* 添加版本号记录

### BuAdPlugin

#### V1.0.0

* 添加版本号记录

### ChartBoostPlugin

#### V1.0.0

* 添加版本号记录

### FacebookPlugin

#### V1.0.0

* 添加版本号记录

### GdtPlugin

#### V1.0.0

* 添加版本号记录

### IronSourcePlugin

#### V1.0.0

* 添加版本号记录

### MoPubPlugin

#### V1.0.0

* 添加版本号记录

### UnityAdsPlugin

#### V1.0.0

* 添加版本号记录

### VunglePlugin

#### V1.0.0

* 添加版本号记录
