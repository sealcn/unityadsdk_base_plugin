package com.meevii.adsdk.splash_plugin;

import android.os.Bundle;
import android.widget.FrameLayout;

import com.unity3d.player.UnityPlayerActivity;

public class ADSDKSplashActivity extends UnityPlayerActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adsdksplash);
        FrameLayout frameLayout = findViewById(R.id.unity_player_layout);
        frameLayout.addView(mUnityPlayer.getView());
        mUnityPlayer.requestFocus();
    }
}
