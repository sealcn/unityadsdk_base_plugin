package com.meevii.adsdk.vungle_plugin;

import android.app.Activity;
import android.content.Context;

import com.meevii.adsdk.base_plugin.adcommon.ADSDKLog;
import com.meevii.adsdk.base_plugin.adcommon.IAdListener;
import com.meevii.adsdk.base_plugin.adcommon.IRewardAdListener;
import com.meevii.adsdk.base_plugin.adcommon.ThreadHandler;
import com.vungle.warren.AdConfig;
import com.vungle.warren.LoadAdCallback;
import com.vungle.warren.PlayAdCallback;
import com.vungle.warren.Vungle;

public class RewardVideo {
    private IAdListener adListener;
    private String mAdUnitId;

    private boolean isGotReward = false;

    public RewardVideo(final Activity unityActivity, final String adUnitId) {
        mAdUnitId = adUnitId;
    }

    public void setAdListener(IAdListener adListener) {
        this.adListener = adListener;
    }

    private void OnAdLoad() {
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdLoaded();
                    }
                }
            }).start();
        }
    }

    private void OnAdFailedToLoad(final String errorMessage, final int errorCode) {
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdFailedToLoad(errorMessage, errorCode);
                    }
                }
            }).start();
        }
    }

    private void OnAdClosed() {
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        if (adListener instanceof IRewardAdListener) {
                            if (isGotReward) {
                                ((IRewardAdListener) adListener).onAdReward();
                            } else {
                                ((IRewardAdListener) adListener).onAdNotReward();
                            }
                        }
                        isGotReward = false;
                        adListener.onAdClosed();
                    }
                }
            }).start();
        }
    }

    public void loadAd(Context context) {

        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                if (Vungle.isInitialized()) {
                    Vungle.loadAd(mAdUnitId, new LoadAdCallback() {
                        @Override
                        public void onAdLoad(String placementReferenceId) {
                            OnAdLoad();
                        }

                        @Override
                        public void onError(String placementReferenceId, Throwable throwable) {
                            if (mAdUnitId != null && mAdUnitId.equals(placementReferenceId)) {
                                OnAdFailedToLoad(throwable.getLocalizedMessage(), -1);
                            }
                        }
                    });
                }
            }
        });
    }

    public void show(Context context) {
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                if (Vungle.canPlayAd(mAdUnitId)) {
                    AdConfig adConfig = new AdConfig();
                    adConfig.setBackButtonImmediatelyEnabled(true);
                    adConfig.setAutoRotate(true);
                    adConfig.setMuted(false);
                    Vungle.setIncentivizedFields("User", "Title", "Body", "RewardedKeepWatching", "RewardedClose");
                    Vungle.playAd(mAdUnitId, adConfig, new PlayAdCallback() {
                        @Override
                        public void onAdStart(String placementReferenceId) {
                        }

                        @Override
                        public void onAdEnd(String placementReferenceId, boolean completed, boolean isCTAClicked) {
                            isGotReward = true;
                            OnAdClosed();
                        }

                        @Override
                        public void onError(String placementReferenceId, Throwable throwable) {
                            ADSDKLog.Log("[Vungle] onError:" + throwable.getLocalizedMessage());
                        }
                    });
                }
            }
        });
    }

    public void destroy(Context context) {
        this.adListener = null;
    }
}