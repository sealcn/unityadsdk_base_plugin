package com.meevii.adsdk.vungle_plugin;

import android.app.Activity;
import android.content.Context;

import com.meevii.adsdk.base_plugin.adcommon.ADSDKLog;
import com.meevii.adsdk.base_plugin.adcommon.BaseBridge;
import com.meevii.adsdk.base_plugin.adcommon.IAdListener;
import com.vungle.warren.InitCallback;
import com.vungle.warren.Vungle;

public class AndroidBridge extends BaseBridge {

    public static String mAppId;
    private static AdObjectFactory objFactorys;

    public static String CreateInstance(Activity activity, String type, String adUintId, IAdListener listener) {
        if (objFactorys == null) {
            objFactorys = new AdObjectFactory();
        }

        return objFactorys.createInstance(activity, type, adUintId, listener);
    }

    public static Object Invoke(Context context, String className, String objUUId, String methodName) {
        if (objFactorys == null)
            return null;
        Object obj = objFactorys.getObject(objUUId);
        if (obj == null) {
            return null;
        }
        return BaseBridge.InvokeMethod(obj, context, className, methodName);
    }

    public static void SetSdk(Context context, String appId) {
        mAppId = appId;
        if (mAppId != null) {
            Vungle.init(mAppId, context.getApplicationContext(), new InitCallback() {
                @Override
                public void onSuccess() {
                    ADSDKLog.Log("[SetSdk] onSuccess!");
                }

                @Override
                public void onError(Throwable throwable) {
                    ADSDKLog.Log("[SetSdk] onError:" + throwable.getLocalizedMessage());
                }

                @Override
                public void onAutoCacheAdAvailable(String s) {
                    ADSDKLog.Log("[SetSdk] onAutoCacheAdAvailable!");
                }
            });
        }
    }

}
