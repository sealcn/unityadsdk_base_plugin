package com.meevii.adsdk.vungle_plugin;

import android.content.Context;

import com.meevii.adsdk.base_plugin.adcommon.ADSDKLog;
import com.meevii.adsdk.base_plugin.adcommon.IAdListener;
import com.meevii.adsdk.base_plugin.adcommon.ThreadHandler;
import com.vungle.warren.LoadAdCallback;
import com.vungle.warren.PlayAdCallback;
import com.vungle.warren.Vungle;

public class Interstitial {

    private IAdListener adListener;
    private String mAdUnitId;

    public Interstitial(final Context context, final String adUnitId) {
        this.mAdUnitId = adUnitId;
    }

    public void setAdListener(IAdListener adListener) {
        this.adListener = adListener;
    }

    public void loadAd(Context context) {
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                if (Vungle.isInitialized()) {
                    Vungle.loadAd(mAdUnitId, new LoadAdCallback() {
                        @Override
                        public void onAdLoad(String placementReferenceId) {
                            OnAdLoad();
                        }

                        @Override
                        public void onError(String placementReferenceId, Throwable throwable) {
                            if (mAdUnitId != null && mAdUnitId.equals(placementReferenceId)) {
                                OnAdFailedToLoad(throwable.getLocalizedMessage(), -1);
                            }
                        }
                    });
                } else {
                    OnAdFailedToLoad("SDK Not Init Finish", -1);
                }
            }
        });
    }

    private void OnAdLoad() {
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdLoaded();
                    }
                }
            }).start();
        }
    }

    private void OnAdFailedToLoad(final String errorMessage, final int errorCode) {
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdFailedToLoad(errorMessage, errorCode);
                    }
                }
            }).start();
        }
    }

    private void OnAdClosed() {
        ADSDKLog.Log("[Vungle] OnAdClosed!");
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdClosed();
                    }
                }
            }).start();
        }
    }

    public void show(Context context) {
        ADSDKLog.Log("[Vungle] OnAdClosed!");
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                if (Vungle.canPlayAd(mAdUnitId)) {
                    Vungle.playAd(mAdUnitId, null, new PlayAdCallback() {
                        @Override
                        public void onAdStart(String placementReferenceId) {
                        }

                        @Override
                        public void onAdEnd(String placementReferenceId, boolean completed, boolean isCTAClicked) {
                            OnAdClosed();
                        }

                        @Override
                        public void onError(String placementReferenceId, Throwable throwable) {
                            ADSDKLog.Log("[Vungle] onError:" + throwable.getLocalizedMessage());
                        }
                    });
                }
            }
        });
    }

    public void destroy(Context context) {
        this.adListener = null;
    }
}
