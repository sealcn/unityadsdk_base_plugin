package com.meevii.adsdk.ironsource_plugin;

import android.app.Activity;
import android.content.Context;

import com.ironsource.mediationsdk.IronSource;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.model.Placement;
import com.ironsource.mediationsdk.sdk.RewardedVideoListener;
import com.meevii.adsdk.base_plugin.adcommon.ADSDKLog;
import com.meevii.adsdk.base_plugin.adcommon.IAdListener;
import com.meevii.adsdk.base_plugin.adcommon.IRewardAdListener;
import com.meevii.adsdk.base_plugin.adcommon.ThreadHandler;

public class RewardVideo {
    private IAdListener adListener;
    private boolean isGotReward = false;

    public RewardVideo(final Activity unityActivity, final String adUnitId, IAdListener adListener) {
        this.adListener = adListener;
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                registerEvent();
            }
        });
    }

    private void registerEvent() {
        ADSDKLog.Log("[IronSource] registerEvent Reward!");
        if (IronSource.isRewardedVideoAvailable()) {
            OnAdLoad();
        }
        IronSource.setRewardedVideoListener(new RewardedVideoListener() {
            @Override
            public void onRewardedVideoAdOpened() {
            }

            @Override
            public void onRewardedVideoAdClosed() {
                OnAdClosed();
            }

            @Override
            public void onRewardedVideoAvailabilityChanged(boolean iaLoaded) {
                if (iaLoaded) {
                    OnAdLoad();
                } else {
                    OnAdFailedToLoad("-1", -1);
                }
            }

            @Override
            public void onRewardedVideoAdStarted() {
            }

            @Override
            public void onRewardedVideoAdEnded() {
                isGotReward = true;
            }

            @Override
            public void onRewardedVideoAdRewarded(Placement placement) {
            }

            @Override
            public void onRewardedVideoAdShowFailed(IronSourceError ironSourceError) {
                OnAdFailedToLoad(ironSourceError.getErrorMessage(), ironSourceError.getErrorCode());
            }

            @Override
            public void onRewardedVideoAdClicked(Placement placement) {
                OnClick();
            }
        });
    }

    private void OnAdLoad() {
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdLoaded();
                    }
                }
            }).start();
        }
    }

    private void OnAdFailedToLoad(final String errorMessage, final int errorCode) {
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdFailedToLoad(errorMessage, errorCode);
                    }
                }
            }).start();
        }
    }

    private void OnAdClosed() {
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        if (adListener instanceof IRewardAdListener) {
                            if (isGotReward) {
                                ((IRewardAdListener) adListener).onAdReward();
                            } else {
                                ((IRewardAdListener) adListener).onAdNotReward();
                            }
                        }
                        isGotReward = false;
                        adListener.onAdClosed();
                    }
                }
            }).start();
        }
    }

    private void OnClick() {
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    adListener.onAdClick();
                }
            }).start();
        }
    }

    public void loadAd(final Context context) {
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                IronSource.init((Activity) context, AndroidBridge.mAppId, IronSource.AD_UNIT.REWARDED_VIDEO);
            }
        });
    }

    public void show(Context context) {
        if (!IronSource.isRewardedVideoAvailable()) {
            return;
        }
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                IronSource.showRewardedVideo();
            }
        });
    }

    public void destroy(Context context) {
        this.adListener = null;
    }
}