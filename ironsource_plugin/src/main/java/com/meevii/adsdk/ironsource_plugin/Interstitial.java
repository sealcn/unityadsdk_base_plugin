package com.meevii.adsdk.ironsource_plugin;

import android.app.Activity;
import android.content.Context;

import com.ironsource.mediationsdk.IronSource;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.sdk.InterstitialListener;
import com.meevii.adsdk.base_plugin.adcommon.ADSDKLog;
import com.meevii.adsdk.base_plugin.adcommon.IAdListener;
import com.meevii.adsdk.base_plugin.adcommon.ThreadHandler;

public class Interstitial {
    private IAdListener adListener;

    public Interstitial(final Context context, final String adUnitId, IAdListener adListener) {
        this.adListener = adListener;
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                registerEvent();
                IronSource.init((Activity) context, AndroidBridge.mAppId, IronSource.AD_UNIT.INTERSTITIAL);
            }
        });
    }

    private void registerEvent() {
        ADSDKLog.Log("[IronSource] registerEvent Inter!");
        if (IronSource.isInterstitialReady()) {
            OnAdLoad();
        }
        IronSource.setInterstitialListener(new InterstitialListener() {
            @Override
            public void onInterstitialAdReady() {
                OnAdLoad();
            }

            @Override
            public void onInterstitialAdLoadFailed(IronSourceError ironSourceError) {
                OnAdFailedToLoad(ironSourceError.getErrorMessage(), ironSourceError.getErrorCode());
            }

            @Override
            public void onInterstitialAdOpened() {
            }

            @Override
            public void onInterstitialAdClosed() {
                OnAdClosed();
            }

            @Override
            public void onInterstitialAdShowSucceeded() {
            }

            @Override
            public void onInterstitialAdShowFailed(IronSourceError ironSourceError) {
                OnAdFailedToLoad(ironSourceError.getErrorMessage(), ironSourceError.getErrorCode());
            }

            @Override
            public void onInterstitialAdClicked() {
                OnAdClicked();
            }
        });
    }

    public void loadAd(final Context context) {
        ADSDKLog.Log("[IronSource] loadAd!");
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                IronSource.loadInterstitial();
            }
        });
    }

    private void OnAdLoad() {
        ADSDKLog.Log("[IronSource] OnAdLoad!");
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdLoaded();
                    }
                }
            }).start();
        }
    }

    private void OnAdFailedToLoad(final String errorMessage, final int errorCode) {
        ADSDKLog.Log("[IronSource] OnAdFailedToLoad: errorCode:" + errorCode + ", errorMessage:" + errorMessage);
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdFailedToLoad(errorMessage, errorCode);
                    }
                }
            }).start();
        }
    }

    private void OnAdClicked() {
        ADSDKLog.Log("[IronSource] OnAdClicked!");
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdClick();
                    }
                }
            }).start();
        }
    }

    private void OnAdClosed() {
        ADSDKLog.Log("[IronSource] OnAdClosed!");
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdClosed();
                    }
                }
            }).start();
        }
    }

    public void show(Context context) {
        ADSDKLog.Log("[IronSource] show");
        if (!IronSource.isInterstitialReady()) {
            return;
        }

        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                IronSource.showInterstitial();
            }
        });
    }

    public void destroy(Context context) {
        this.adListener = null;
    }
}
