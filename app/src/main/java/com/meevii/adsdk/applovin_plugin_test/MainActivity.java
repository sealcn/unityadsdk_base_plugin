package com.meevii.adsdk.applovin_plugin_test;

import android.app.Activity;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;

import com.meevii.adsdk.facebook_plugin.AndroidBridge;
import com.meevii.adsdk.facebook_plugin.Banner;
import com.meevii.adsdk.facebook_plugin.Interstitial;

import java.lang.reflect.Method;

public class MainActivity extends AppCompatActivity {

    /*
    class DefaultListener implements UnityAdListener{

        @Override
        public void onAdLoaded() {
            int k = 0;
            ++k;
        }

        @Override
        public void onAdFailedToLoad(String errorReason) {
            int k = 0;
            ++k;
        }

        @Override
        public void onAdOpened() {
            int k = 0;
            ++k;
        }

        @Override
        public void onAdClosed() {
            int k = 0;
            ++k;
        }

        @Override
        public void onAdClick() {

        }

        @Override
        public void onAdLeftApplication() {
            int k = 0;
            ++k;
        }
    }

    class InterstitalListener extends DefaultListener{
        Interstitial ad;
        String adUnitId;
        public void setAd(Interstitial ad, String adUintId){
            this.ad = ad;
            this.adUnitId = adUintId;
        }
        public void onAdLoaded() {
            System.out.println("[applovin] " + adUnitId + " onAdLoaded");
            //ad.show();
        }

        public void onAdFailedToLoad(String errorReason) {
            System.out.println("[applovin] " + adUnitId + " onAdFailedToLoad: " + errorReason);
        }
    }

    Banner createBanner(final String adUnitId){
        Banner banner = new Banner(this, new DefaultListener() );
        banner.create(adUnitId, AdSize.BANNER);
        return banner;
    }

    private Interstitial createInterstitial(String adUnitId) {
        InterstitalListener listener = new InterstitalListener();
        Interstitial interstitial = new Interstitial(this, listener);
        listener.setAd(interstitial, adUnitId);
        interstitial.create(adUnitId);

        return interstitial;
    }

    class RewardVideoListener extends DefaultListener{
        String uuid;
        public void setAd(String uuid){
            this.uuid = uuid;
        }
        public void onAdLoaded() {
            AndroidBridge.Invoke(RewardVideo.class.getName(), uuid, "show");
        }
    }

    class RewardVideoListener2 extends DefaultListener{
        RewardVideo ad;
        public void setAd(RewardVideo ad){
            this.ad = ad;
        }
        public void onAdLoaded() {
            ad.show();
        }
    }

    private RewardVideo createRewardVideo(String adUnitId) {
        RewardVideoListener2 listener = new RewardVideoListener2();
        RewardVideo rewardVideo = new RewardVideo(this, listener);
        listener.setAd(rewardVideo);
        rewardVideo.create(adUnitId);

        return rewardVideo;
    }
*/
    String uuid = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AndroidBridge.addTestDevice(AndroidBridge.getTestDeviceID(this));

        uuid = AndroidBridge.CreateInstance(this, Interstitial.class.getName(), "YOUR_PLACEMENT_ID", new com.meevii.adsdk.base_plugin.adcommon.IAdListener() {
            @Override
            public void onAdLoaded() {
                System.out.println("onAdLoaded");
                AndroidBridge.Invoke(MainActivity.this, Interstitial.class.getName(), uuid, "show");
            }

            @Override
            public void onAdFailedToLoad(String errorReason, int rawErrorCode) {
                System.out.println("onAdFailedToLoad");
            }

            @Override
            public void onAdClick() {
                System.out.println("onAdClick");
            }

            @Override
            public void onAdOpened() {
                System.out.println("onAdOpened");
            }

            @Override
            public void onAdClosed() {
                System.out.println("onAdClosed");
            }

            @Override
            public void onAdLeftApplication() {
                System.out.println("onAdLeftApplication");
            }
        });
        AndroidBridge.Invoke(MainActivity.this, Interstitial.class.getName(), uuid, "loadAd");
    }


    public static int getRealSceenHeightPix(Activity ctx){
        int height = 0;
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR1){ //17
            DisplayMetrics dm = new DisplayMetrics();
            ctx.getWindowManager().getDefaultDisplay().getRealMetrics(dm);
            height = dm.heightPixels;
        }else{
            Display display = ctx.getWindowManager().getDefaultDisplay();
            DisplayMetrics dm = new DisplayMetrics();
            try {
                Class c = Class.forName("android.view.Display");
                Method method = c.getMethod("getRealMetrics",DisplayMetrics.class);
                method.invoke(display, dm);
                height = dm.heightPixels;
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        //if (height == 0){
        //    DisplayMetrics dm = ctx.getResources().getDisplayMetrics();
        //    height = dm.heightPixels;
        //}
        return height;
    }


}
