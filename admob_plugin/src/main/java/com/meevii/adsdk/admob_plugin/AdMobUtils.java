package com.meevii.adsdk.admob_plugin;

import android.content.Context;
import android.provider.Settings;
import android.text.TextUtils;

import com.google.android.gms.ads.AdRequest;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Locale;

public class AdMobUtils {
    public static String getErrorReason(int errorCode) {
        switch (errorCode) {
            case AdRequest.ERROR_CODE_INTERNAL_ERROR:
                return "Internal Error, Admob Error";
            case AdRequest.ERROR_CODE_INVALID_REQUEST:
                return "Invalid Request, Maybe Ad Error";
            case AdRequest.ERROR_CODE_NETWORK_ERROR:
                return "Network Error";
            case AdRequest.ERROR_CODE_NO_FILL:
                return "No Fill";
            default:
                return "";
        }
    }

    public static String getAdMobTestDeviceID(Context ctx) {
        //see:
        //com.google.android.gms.internal.ads.zzamu.zzbc

        String android_id = Settings.Secure.getString(ctx.getContentResolver(), "android_id");
        if (TextUtils.isEmpty(android_id))
            return "";
        try {
            MessageDigest var2;
            (var2 = MessageDigest.getInstance("MD5")).update(android_id.getBytes());
            return String.format(Locale.US, "%032X", new BigInteger(1, var2.digest()));
        } catch (Exception var3) {
        }
        return "";
    }
}
