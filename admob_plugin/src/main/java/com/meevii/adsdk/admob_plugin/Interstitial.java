package com.meevii.adsdk.admob_plugin;

import android.content.Context;
import android.text.TextUtils;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.meevii.adsdk.base_plugin.adcommon.ADSDKLog;
import com.meevii.adsdk.base_plugin.adcommon.IAdListener;
import com.meevii.adsdk.base_plugin.adcommon.ThreadHandler;

public class Interstitial {

    private InterstitialAd mInterstitialAd;
    private IAdListener adListener;
    private boolean isLoaded;
    private String mAdUnitId;
    private String mStrTestDeviceID;

    public Interstitial(final Context context, final String adUnitId) {
        this.mAdUnitId = adUnitId;
        this.isLoaded = false;

        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                mInterstitialAd = new InterstitialAd(context);
                mInterstitialAd.setAdUnitId(mAdUnitId);
                registerEvent();
            }
        });
    }

    public void setAdListener(IAdListener adListener) {
        this.adListener = adListener;
    }

    private void registerEvent() {
        mInterstitialAd.setAdListener(new AdListener() {

            public void onAdLoaded() {
                OnAdLoad();
            }

            public void onAdFailedToLoad(int errorCode) {
                OnAdFailedToLoad(AdMobUtils.getErrorReason(errorCode), errorCode);
            }

            public void onAdLeftApplication() {
                OnAdLeftApplication();
            }

            public void onAdOpened() {
            }

            public void onAdClicked() {
                OnAdClicked();
            }

            public void onAdImpression() {
            }

            public void onAdClosed() {
                OnAdClosed();
            }

        });
    }

    public void loadAd(Context context) {

        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                if (mInterstitialAd != null) {

                    AdRequest.Builder builder = new AdRequest.Builder();

                    if (!TextUtils.isEmpty(mStrTestDeviceID)) {
                        ADSDKLog.Log("[admob] addTestDevice: " + mStrTestDeviceID);
                        builder.addTestDevice(mStrTestDeviceID);
                    }
                    mInterstitialAd.loadAd(builder.build());
                }
            }
        });
    }

    private void OnAdLoad() {
        isLoaded = true;
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdLoaded();
                    }
                }
            }).start();
        }
    }

    private void OnAdFailedToLoad(final String errorMessage, final int errorCode) {
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdFailedToLoad(errorMessage, errorCode);
                    }
                }
            }).start();
        }
    }

    private void OnAdClicked() {
        ADSDKLog.Log("[admob] OnAdClicked!");
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdClick();
                    }
                }
            }).start();
        }
    }

    private void OnAdClosed() {
        ADSDKLog.Log("[admob] OnAdClosed!");
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdClosed();
                    }
                }
            }).start();
        }
    }

    private void OnAdLeftApplication() {
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdLeftApplication();
                    }
                }
            }).start();
        }
    }

    /**
     * Returns {@code True} if the interstitial has loaded.
     */
    public boolean isLoaded(Context context) {
        return isLoaded;
    }

    /**
     * Shows the interstitial if it has loaded.
     */
    public void show(Context context) {
        ADSDKLog.Log("[Admob] show");
        if (!isLoaded(null))
            return;
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                if (isLoaded(null)) {
                    mInterstitialAd.show();
                }
            }
        });
    }

    public void destroy(Context context) {
        this.adListener = null;
        isLoaded = false;
    }

    public boolean isValid(Context context) {
        if (mInterstitialAd == null)
            return false;
        return isLoaded && mInterstitialAd.isLoaded();
    }

    public void setTestDevice(Context context, String strTestDeviceID) {
        if (!TextUtils.isEmpty(strTestDeviceID)) {
            mStrTestDeviceID = strTestDeviceID;
        } else {
            mStrTestDeviceID = AdMobUtils.getAdMobTestDeviceID(context);
        }
    }
}
