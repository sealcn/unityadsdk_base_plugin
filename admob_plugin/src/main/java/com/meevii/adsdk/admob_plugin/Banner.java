package com.meevii.adsdk.admob_plugin;

import android.content.Context;
import android.view.View;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.meevii.adsdk.base_plugin.adcommon.ADSDKLog;
import com.meevii.adsdk.base_plugin.adcommon.AdBanner;
import com.meevii.adsdk.base_plugin.adcommon.AdBannerSize;

public class Banner extends AdBanner {

    AdView mAdView;
    private Context mContext;
    private String mAdId;
    private AdBannerSize mAdSize;

    private Banner(Context context, AdBannerSize adSize) {
        super(context, adSize);
    }

    public Banner(Context applicationContext, String adUnitId, AdBannerSize adSize) {
        super(applicationContext, adSize);
        create(adUnitId);
    }

    @Override
    protected View CreateInstance(Context context, String adId, AdBannerSize adSize) {
        mContext = context;
        mAdId = adId;
        mAdSize = adSize;
        AdSize admobAdSize = AdSize.BANNER;
        if (adSize.equals(AdBannerSize.SMART_BANNER)) {
            admobAdSize = AdSize.SMART_BANNER;
        }
        mAdView = new AdView(context);
        mAdView.setAdUnitId(adId);
        mAdView.setAdSize(admobAdSize);
        return mAdView;
    }

    @Override
    protected void setListener(View adView) {
        if (mAdView != null) {
            mAdView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    Banner.this.onAdLoaded();
                }

                @Override
                public void onAdFailedToLoad(int errorCode) {
                    Banner.this.onAdFailedToLoad(AdMobUtils.getErrorReason(errorCode), errorCode);
                }

                @Override
                public void onAdOpened() {
                    ADSDKLog.Log("[admob] OnAdClicked!");
                    Banner.this.onAdClick();
                }

                @Override
                public void onAdLeftApplication() {
                    Banner.this.onAdLeftApplication();
                }

                @Override
                public void onAdClosed() {
                    Banner.this.onAdClosed();
                }

                @Override
                public void onAdClicked() {
                    Banner.this.onAdClick();
                }
            });
        }
    }

    @Override
    protected void doLoadAd(View adView) {
        AdRequest adRequest = new AdRequest.Builder().build();
        if (mAdView != null) {
            mAdView.loadAd(adRequest);
        }
    }

    @Override
    protected void doDestroy(View adView) {
        if (mAdView != null) {
            mAdView.setAdListener(null);
            mAdView.destroy();
        }
        mAdView = null;
    }

    @Override
    protected void resumeAdView(View adView) {
    }

    @Override
    protected void pauseAdView(View adView) {
    }
}
