package com.meevii.adsdk.admob_plugin;

import android.app.Activity;
import android.content.Context;

import com.meevii.adsdk.base_plugin.adcommon.BaseBridge;
import com.meevii.adsdk.base_plugin.adcommon.IAdListener;

public class AndroidBridge extends BaseBridge {

    private static AdObjectFactory objFactorys;

    public static String CreateInstance(Activity activity, String type, String adUintId, IAdListener listener) {
        if (objFactorys == null) {
            objFactorys = new AdObjectFactory();
        }

        return objFactorys.createInstance(activity, type, adUintId, listener);
    }

    public static Object Invoke(Context context, String className, String objUUId, String methodName) {
        if (objFactorys == null)
            return null;
        Object obj = objFactorys.getObject(objUUId);
        if (obj == null) {
            return null;
        }
        return BaseBridge.InvokeMethod(obj, context, className, methodName);
    }

    public static void AddTestDevice(Context context, String objUUId, String strTestDeviceID) {
        if (objFactorys == null) {
            return;
        }
        Object obj = objFactorys.getObject(objUUId);
        if (obj == null) {
            return;
        }

        if (obj instanceof Interstitial) {
            ((Interstitial) obj).setTestDevice(context, strTestDeviceID);
        } else if (obj instanceof RewardVideo) {
            ((RewardVideo) obj).setTestDevice(context, strTestDeviceID);
        }
    }
}
