package com.meevii.adsdk.admob_plugin;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.rewarded.RewardItem;
import com.google.android.gms.ads.rewarded.RewardedAd;
import com.google.android.gms.ads.rewarded.RewardedAdCallback;
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback;
import com.meevii.adsdk.base_plugin.adcommon.ADSDKLog;
import com.meevii.adsdk.base_plugin.adcommon.IAdListener;
import com.meevii.adsdk.base_plugin.adcommon.IRewardAdListener;
import com.meevii.adsdk.base_plugin.adcommon.ThreadHandler;

public class RewardVideo {
    private Activity mUnityActivity;
    private RewardedAd mRewardAd;
    private IAdListener adListener;
    private boolean isLoaded;
    private String mStrTestDeviceID;
    private String mAdUnitId;

    private boolean isGotReward = false;

    public RewardVideo(final Activity unityActivity, final String adUnitId) {
        mAdUnitId = adUnitId;
        isLoaded = false;
        mUnityActivity = unityActivity;

        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                mRewardAd = new RewardedAd(unityActivity, mAdUnitId);
            }
        });
    }

    public void setAdListener(IAdListener adListener) {
        this.adListener = adListener;
    }

    private void OnAdLoad() {
        isLoaded = true;
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdLoaded();
                    }
                }
            }).start();
        }
    }

    private void OnAdFailedToLoad(final String errorMessage, final int errorCode) {
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdFailedToLoad(errorMessage, errorCode);
                    }
                }
            }).start();
        }
    }

    private void OnAdClosed() {
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        if (adListener instanceof IRewardAdListener) {
                            if (isGotReward) {
                                ((IRewardAdListener) adListener).onAdReward();
                            } else {
                                ((IRewardAdListener) adListener).onAdNotReward();
                            }
                        }
                        isGotReward = false;
                        adListener.onAdClosed();
                    }
                }
            }).start();
        }
    }

    private void OnAdOpened() {
        if (adListener != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (adListener != null) {
                        adListener.onAdOpened();
                    }
                }
            }).start();
        }
    }

    public void loadAd(Context context) {

        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                if (mRewardAd != null) {

                    AdRequest.Builder builder = new AdRequest.Builder();

                    if (!TextUtils.isEmpty(mStrTestDeviceID)) {
                        ADSDKLog.Log("[admob] addTestDevice: " + mStrTestDeviceID);
                        builder.addTestDevice(mStrTestDeviceID);
                    }

                    mRewardAd.loadAd(builder.build(), new RewardedAdLoadCallback() {
                        @Override
                        public void onRewardedAdLoaded() {
                            OnAdLoad();
                        }

                        @Override
                        public void onRewardedAdFailedToLoad(int errorCode) {
                            OnAdFailedToLoad(AdMobUtils.getErrorReason(errorCode), errorCode);
                        }
                    });
                }
            }
        });
    }

    public boolean isLoaded(Context context) {
        return isLoaded;
    }

    public void show(Context context) {
        if (!isLoaded(null))
            return;
        ThreadHandler.executeOnMainThread(new ThreadHandler.IHander() {
            @Override
            public void onHandler() {
                mRewardAd.show(mUnityActivity, new RewardedAdCallback() {

                    @Override
                    public void onRewardedAdOpened() {
                        OnAdOpened();
                    }

                    @Override
                    public void onRewardedAdClosed() {
                        OnAdClosed();
                    }

                    @Override
                    public void onUserEarnedReward(@NonNull RewardItem rewardItem) {
                        isGotReward = true;
                    }

                    @Override
                    public void onRewardedAdFailedToShow(int errorCode) {
                        ADSDKLog.Log("[Admob] errorCode:" + errorCode);
                    }
                });
            }
        });
    }

    public void destroy(Context context) {
        mRewardAd = null;
        this.adListener = null;
        this.isLoaded = false;
    }

    public boolean isValid(Context context) {
        if (mRewardAd == null) {
            return false;
        }
        return isLoaded && mRewardAd.isLoaded();
    }

    public void setTestDevice(Context context, String strTestDeviceID) {
        if (!TextUtils.isEmpty(strTestDeviceID)) {
            mStrTestDeviceID = strTestDeviceID;
        } else {
            mStrTestDeviceID = AdMobUtils.getAdMobTestDeviceID(context);
        }
    }
}